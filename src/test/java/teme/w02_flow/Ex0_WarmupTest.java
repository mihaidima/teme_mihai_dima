package teme.w02_flow;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import teme.util.plugin.Grade;
import teme.util.plugin.GradeRunner;

import static org.junit.Assert.*;
import static teme.w02_flow.Ex0_Warmup.*;

/**
 * MAX GRADE: 16p+4p
 */
@RunWith(GradeRunner.class)
public class Ex0_WarmupTest {

    private final double DELTA = 0.0001; //precision using when comparing double values for tests

    @Rule
    public Timeout globalTimeout = Timeout.seconds(10); // max running time allowed per each test method

    @Test
    @Grade(1)
    public void testXTimes() {
        assertEquals("", xTimes("", 0));
        assertEquals("", xTimes("abc", 0));
        assertEquals("abcabc", xTimes("abc", 2));
        assertEquals("AAA", xTimes("A", 3));
    }

    @Test
    @Grade(1)
    public void testSumSquares() {
        assertEquals(0, sumSquares(0));
        assertEquals(1, sumSquares(1));
        assertEquals(5, sumSquares(2));
        assertEquals(385, sumSquares(10));
    }


    @Test
    @Grade(1)
    public void testFactorial_valuesUpTo12_usingIntTypeIsEnough() {
        assertEquals(1, factorial(1));
        assertEquals(2, factorial(2));
        assertEquals(120, factorial(5));
        assertEquals(3628800, factorial(10));
        assertEquals(479001600, factorial(12));
    }

    @Test
    @Grade(1)
    public void testFactorial_valuesUpTo_16_requireUseOfLongType() {
        assertEquals(6227020800L, factorial(13));
        assertEquals(87178291200L, factorial(14));
        assertEquals(20922789888000L, factorial(16)); //about the max value that still fits in the range for long!
    }

    @Test
    @Grade(1)
    public void testFactorial_shouldNotUseRecursion() {
        try {
            System.out.println(factorial(100_000));
        } catch (StackOverflowError ignore) {
            fail("For big numbers (like 100000), factorial() - the iterative version - is not expected to produce correct results, BUT it should NOT fail with StackOverflowError.\nYour version just fails in this way. Is it really iterative?... (seems recursive to me)\n");
        }
    }

    @Test
    @Grade(2)
    public void testFactorialRec_valuesUpTo12_usingIntTypeIsEnough() {
        assertEquals(1, factorialRec(1));
        assertEquals(2, factorialRec(2));
        assertEquals(120, factorialRec(5));
        assertEquals(3628800, factorialRec(10));
        assertEquals(479001600, factorialRec(12));
    }

    @Test
    @Grade(1)
    public void testFactorialRec_valuesUpTo_16_requireUseOfLongType() {
        assertEquals(6227020800L, factorialRec(13));
        assertEquals(87178291200L, factorialRec(14));
        assertEquals(20922789888000L, factorialRec(16)); //about the max value that still fits in the range for long!
    }

    @Test
    @Grade(1)
    public void testFactorialRec_shouldUseRecursion() {
        try {
            System.out.println(factorialRec(100_000));
            fail("For big numbers (like 100000), factorialRec() - the recursive version - is expected to just fail with StackOverflowError.\nYour version didn't fail like that! Is it really recursive?... (seems iterative to me)\n");
        } catch (StackOverflowError ignore) {
            //ok, expected
        }
    }


    @Test
    @Grade(1)
    public void testDayOfWeek_lowerCaseOnly() {
        assertEquals(1, dayOfWeek("luni"));
        assertEquals(2, dayOfWeek("Marti"));
        assertEquals(3, dayOfWeek("MIERCURI"));
        assertEquals(4, dayOfWeek("jOi"));
        assertEquals(5, dayOfWeek("Vineri"));
        assertEquals(6, dayOfWeek("SaMbAtA"));
        assertEquals(7, dayOfWeek("duminica"));
        assertEquals(-1, dayOfWeek("altceva"));
        assertEquals(-1, dayOfWeek(""));
    }

    @Test
    @Grade(1)
    public void testDayOfWeek_mixedCase() {
        assertEquals(1, dayOfWeek("lUni"));
        assertEquals(2, dayOfWeek("Marti"));
        assertEquals(3, dayOfWeek("MiERCURi"));
        assertEquals(4, dayOfWeek("jOi"));
        assertEquals(5, dayOfWeek("VinerI"));
        assertEquals(6, dayOfWeek("SaMbAtA"));
        assertEquals(7, dayOfWeek("Duminica"));
    }


    @Test
    @Grade(1)
    public void testSum() {
        assertEquals(0, sum(new double[]{}), DELTA);
        assertEquals(-4, sum(new double[]{-4}), DELTA);
        assertEquals(2, sum(new double[]{1, 2, 3, -4}), DELTA);
    }


    @Test
    @Grade(1)
    public void testAvg_emptyArray() {
        double avgEmpty = avg(new double[]{});
        assertTrue(Double.isNaN(avgEmpty) || avgEmpty == 0);
    }

    @Test
    @Grade(1)
    public void testAvg_nonEmptyArray() {
        assertEquals(2, avg(new double[]{2}), DELTA);
        assertEquals(2.75, avg(new double[]{1.1, 2.2, 3.3, 4.4}), DELTA);
    }


    @Test
    @Grade(1)
    public void testMax_emptyArray() {
        assertEquals(Double.NEGATIVE_INFINITY, max(new double[]{}), DELTA);
    }

    @Test
    @Grade(1)
    public void testMax_nonEmptyArray() {
        //one element
        assertEquals(5, max(new double[]{5}), DELTA);
        assertEquals(-4, max(new double[]{-4}), DELTA);
        assertEquals(0, max(new double[]{0}), DELTA);

        //multiple elements
        assertEquals(3, max(new double[]{1, 2, 3, -4}), DELTA);
        assertEquals(4, max(new double[]{1, 2, 3, 4}), DELTA);
        assertEquals(4, max(new double[]{4, 3, 2, 1}), DELTA);
        assertEquals(5, max(new double[]{4, 5, 1, 2, 3}), DELTA);
        assertEquals(6, max(new double[]{4, 3, 2, 6, 5, 1}), DELTA);
        assertEquals(0, max(new double[]{-3, -1, 0, -2}), DELTA);
        assertEquals(-1, max(new double[]{-2, -1, -3}), DELTA);
        assertEquals(-20, max(new double[]{-20, -30}), DELTA);
    }


    @Test
    @Grade(1)
    public void testSumPositives() {
        assertEquals(0, sumPositives(new double[]{}), DELTA);
        assertEquals(0, sumPositives(new double[]{-1, 2, 3}), DELTA);
        assertEquals(6, sumPositives(new double[]{1, 2, 3, -4, 15}), DELTA);
        assertEquals(0, sumPositives(new double[]{-1, 2, 3}), DELTA);
    }

    @Test
    @Grade(1)
    public void testSumPositives_shouldNotStopAtJustZero() {
        assertEquals(10, sumPositives(new double[]{1, 2, 0, 3, 4, -1, 5, 6}), DELTA);
    }


    @Test
    @Grade(1)
    public void testMultiply_emptyArray() {
        double[] arr = {};
        multiply(arr, 2.5);
        assertArrayEquals(new double[]{}, arr, DELTA);
    }

    @Test
    @Grade(1)
    public void testMultiply_nonEmptyArray() {
        double[] arr = {10, 20, 30, 40, 50};
        multiply(arr, 2);
        assertArrayEquals(new double[]{20, 40, 60, 80, 100}, arr, DELTA);
        multiply(arr, 0);
        assertArrayEquals(new double[]{0, 0, 0, 0, 0}, arr, DELTA);
    }
}
