package teme.w13_jdbc;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DogDaoTest {

    @BeforeClass
    public static void setupDb() throws SQLException {
        DogDDL.createTable();
    }


    @Before
    public void cleanUp() throws SQLException {
        DogDao.deleteAll();
    }

    @Test
    public void getAll() throws SQLException {
        List<DogDTO> dogs = DogDao.getAll();
        assertEquals(0, dogs.size());

        DogDao.insert(new DogDTO(2, "Azor", 5));
        dogs = DogDao.getAll();
        assertEquals(1, dogs.size());
        assertEquals("Azor", dogs.get(0).getName());
        assertEquals(5, dogs.get(0).getAge());
    }

    @Test
    public void insert() {

    }

    @Test
    public void deleteAll() {
    }

    @Test
    public void get() {
    }
}