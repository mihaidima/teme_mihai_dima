package teme.w04_oop1.ex6_arraylist;

import org.junit.Test;
import org.junit.runner.RunWith;
import teme.util.plugin.Grade;
import teme.util.plugin.GradeRunner;

import static org.junit.Assert.*;

/**
 * MAX GRADE: 12p
 */
@RunWith(GradeRunner.class)
public class MyArrayListTest {

    @Test
    @Grade(2)
    public void size_add() {
        MyArrayList l = new MyArrayList();
        assertEquals(0, l.size());

        l.add(99);
        l.add(33);
        l.add(99);
        assertEquals(3, l.size());
    }

    @Test
    @Grade(2)
    public void add_get() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);
        assertEquals(10, l.get(0));
        assertEquals(20, l.get(1));
        assertEquals(30, l.get(2));
        assertEquals(40, l.get(3));
    }

    @Test
    @Grade(1)
    public void set() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);
        assertEquals(20, l.get(1));
        l.set(1, 22);
        assertEquals(22, l.get(1));
    }

    @Test
    @Grade(1)
    public void addAtIndex() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);
        l.add(1, 15);
        assertEquals(5, l.size());
        assertEquals(15, l.get(1));
        assertEquals(20, l.get(2));
    }

    @Test
    @Grade(2)
    public void remove() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);

        l.remove();
        assertEquals(3, l.size());
        assertEquals(10, l.get(0));
        assertEquals(20, l.get(1));
        assertEquals(30, l.get(2));

        l.remove(0);
        assertEquals(2, l.size());
        assertEquals(20, l.get(0));
        assertEquals(30, l.get(1));
    }

    @Test
    @Grade(2)
    public void indexOf() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);
        assertEquals(1, l.indexOf(20));
        assertEquals(-1, l.indexOf(70));
    }

    @Test
    @Grade(1)
    public void contains() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        l.add(40);
        assertTrue(l.contains(20));
        assertFalse(l.contains(70));
    }

    @Test
    @Grade(1)
    public void testToString() {
        MyArrayList l = new MyArrayList();
        l.add(10);
        l.add(20);
        l.add(30);
        assertTrue(l.toString().contains("10"));
        assertTrue(l.toString().contains("20"));
        assertTrue(l.toString().contains("30"));
    }
}