package rsd_application;

public class User {
    private String ID;
    private String pass;

    public User(String ID, String pass) {
        this.ID = ID;
        this.pass = pass;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
