package rsd_application;

import rsd_application.dto.Priority;
import rsd_application.dto.RSDDTO;
import rsd_application.dto.State;
import rsd_application.service.RSDDAO;
import rsd_application.service.RSDDDL;

import java.sql.SQLException;

public class RSDMain {
    public static void main(String[] args) throws SQLException {
        RSDDDL.createTable();
        RSDDAO dao = new RSDDAO();
        RSDDTO n1 = new RSDDTO(0, "O descriere",
                null, Priority.LOW, State.ACTIVE);
        dao.insert(n1);
        System.out.println(dao.getAll());

    }
}
