package rsd_application;

public class Supplier {
    private String name;
    private String cdm;

    public Supplier(String name, String cdm) {
        this.name = name;
        this.cdm = cdm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCdm() {
        return cdm;
    }

    public void setCdm(String cdm) {
        this.cdm = cdm;
    }
}
