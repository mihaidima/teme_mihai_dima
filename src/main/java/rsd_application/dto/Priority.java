package rsd_application.dto;

public enum Priority {
    HIGH, MEDIUM, LOW;

    public static Priority getByOrdinal(int ordinal) {
        for (Priority p : values()) {
            if (p.ordinal() == ordinal)
                return p;
        }
        return null;
    }
}
