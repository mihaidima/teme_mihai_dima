package rsd_application.dto;

public enum State {
    ACTIVE, COMPLETED;

    public static State getByOrdinal(int ordinal) {
        for (State s : values()) {
            if (s.ordinal() == ordinal)
                return s;
        }
        return null;
    }
}
