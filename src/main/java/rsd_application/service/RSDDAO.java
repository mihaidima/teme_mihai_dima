package rsd_application.service;

import rsd_application.dto.Priority;
import rsd_application.dto.RSDDTO;
import rsd_application.dto.State;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RSDDAO extends AbstractDao<RSDDTO> {

    @Override
    protected List<RSDDTO> executeSelect(Connection connection) {
        String sql = "SELECT * FROM notes";
        List<RSDDTO> results = new ArrayList<>();
        try {
            PreparedStatement pStatement = connection.prepareStatement(sql);
            ResultSet rs = pStatement.executeQuery();

            while (rs.next()) {
                RSDDTO n = new RSDDTO(rs.getInt("id"),
                        rs.getString("description"),
                        rs.getDate("dueDate"),
                        Priority.getByOrdinal(rs.getInt("priority")),
                        State.getByOrdinal(rs.getInt("state")));
                results.add(n);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    protected RSDDTO executeSelect(Connection connection, long id) {
        return null;
    }

    @Override
    protected void executeInsert(Connection connection, RSDDTO object) {
        String sql = "INSERT INTO notes (DESCRIPTION, DUEDATE, PRIORITY, STATE)" +
                " VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, object.getDescription());
            pStatement.setDate(2, object.getDueDate());
            pStatement.setInt(3, object.getPriority().ordinal());
            pStatement.setInt(4, object.getState().ordinal());
            pStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection connection, RSDDTO object) {

    }

    @Override
    protected void executeDelete(Connection connection, long id) {

    }
}
