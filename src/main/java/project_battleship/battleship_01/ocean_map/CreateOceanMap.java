package project_battleship.battleship_01.ocean_map;

import project_battleship.battleship_01.BattleShipsLauncher;

public class CreateOceanMap extends BattleShipsLauncher {

    public static void main() {

        //creating the game play grid
        for (int i = 0; i < NUMROWS; i++) {
            for (int j = 0; j < NUMCOLS; j++) {
                grid.put(Integer.parseInt(String.format("%d%d", i, j)), "" + i + j);
            }
        }

        //Upper X-coordinates creation
        for (int i = 0; i < NUMCOLS; i++) {
            System.out.print("    " + i);
        }
        System.out.println();
        for (int i = 0; i < NUMCOLS; i++) {
            System.out.print("_____");
        }
        System.out.println();

        //middle side of the map
        for (int i = 0; i < NUMROWS - 1; i++) {
            System.out.print(i + "|");
            for (int j = 0; j < NUMCOLS; j++) {
                System.out.print(" " + grid.getOrDefault(Integer.parseInt(String.format("%d%d", i, j)), "" + i + j) + " |");
            }
            System.out.println();

            System.out.print(" ");
            for (int j = 1; j < NUMCOLS - 1; j++) {
                System.out.print("------");
            }
            System.out.println();
        }

        for (int i = NUMROWS - 1; i < NUMROWS; i++) {
            System.out.print(i + "|");
            for (int j = 0; j < NUMCOLS; j++) {
                System.out.print(" " + i + j + " |");
            }
            System.out.println();

        }
        for (int i = NUMROWS - 1; i < NUMROWS; i++) {
            for (int j = 1; j < NUMCOLS; j++) {
                System.out.print("======");
            }
        }
        System.out.println();

        //Lower X-coordinates creation
        for (int i = 0; i < NUMROWS; i++) {
            System.out.print("    " + i);
        }
        System.out.println();
    }
}
