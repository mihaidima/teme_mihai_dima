package project_battleship.battleship_01;

import project_battleship.battleship_01.create_ships.ComputerShips;
import project_battleship.battleship_01.create_ships.PlayerShips;
import project_battleship.battleship_01.ocean_map.CreateOceanMap;

import java.util.Map;
import java.util.TreeMap;

/*
pot folosi Threads class ca sa creez ambele harti concomitent?
    - player 1
    - player 2
 */


public class BattleShipsLauncher {

    protected final static int NUMROWS = 10;
    protected final static int NUMCOLS = 10;
    protected static Map<Integer, String> grid = new TreeMap<>();

    public static void main(String[] args) {
        CreateOceanMap.main();
        //PlayerShips.main();
        //ComputerShips.main();
    }
}
