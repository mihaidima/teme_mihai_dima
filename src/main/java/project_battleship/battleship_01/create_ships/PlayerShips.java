package project_battleship.battleship_01.create_ships;

import project_battleship.battleship_01.BattleShipsLauncher;

import java.util.Scanner;

public class PlayerShips extends BattleShipsLauncher {
    /*Create ships
        Legend:
        1x Ca = Carrier (contains 5 cells)
        2x Ba = Battleship (contains 4 cells)
        2x Cr = Cruiser (contains 3 cells)
        2x De = Destroyer (contains 2 cells)
        2x Su = Submarines (contains 2 cells)
     */

    public static void main() {
        Scanner point = new Scanner(System.in);
        boolean controlPoint = false;
        int startPoint;
        for (int i = 1; i <= 5; i++) {
            System.out.print("Carrier have 5 cells, provide the cell no:" + i);
            while (!controlPoint) {
                startPoint = point.nextInt();
                if (grid.containsKey(startPoint)) {
                    grid.put(startPoint, "XX");
                    controlPoint = true;
                } else {
                    System.out.println("Provide a point between 00 and 99");
                }
            }
        }
    }
}
