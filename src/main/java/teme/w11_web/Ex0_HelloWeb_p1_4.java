package teme.w11_web;

import spark.Request;

import static spark.Spark.get;

public class Ex0_HelloWeb_p1_4 {

    public static void main(String[] args) {

        //basic pages
        get("/hello1", (req, res) -> hello1_basicPage());
        get("/hello2", (req, res) -> hello2_pageWithCss());

        //reading params
        get("/hello3/:name", (req, res) -> hello3_pageReadingPathParams(req));
        get("/hello4", (req, res) -> hello4_pageReadingQueryParams(req));

        System.out.println("Server started, see: http://localhost:4567/hello1 \n" +
                " http://localhost:4567/hello2 \n" +
                " http://localhost:4567/hello3/YOUR_NAME \n" +
                " http://localhost:4567/hello4?name=YOUR_NAME \n");
    }

    private static String hello1_basicPage() {
        return "Hello World (Wide Web)!";
    }

    private static String hello2_pageWithCss() {
        return "<div style='background-color:#FFFF00; color: blue; border: 3px solid red; padding: 10px;'>" +
                "<i> <h1>Hello World (Wide Web)!</h1></i>" +
                "</div>" +
                "<div style='background-color:blue; color:white; border: 1px solid black;'>" +
                "<h3>Bye world...</h3>" +
                "</div>";
    }

    private static String hello3_pageReadingPathParams(Request request) {
        String nameParam = request.params("name"); //from path

        return nameParam == null || nameParam.isEmpty() ?
                "Hello, stranger!" :
                "Hello, " + nameParam + ", nice to meet you!";
    }

    private static String hello4_pageReadingQueryParams(Request request) {
        String nameParam = request.queryParams("name"); //from query

        return nameParam == null || nameParam.isEmpty() ?
                "Hello, stranger!" :
                "Hello, " + nameParam + ", nice to meet you!";
    }
}
