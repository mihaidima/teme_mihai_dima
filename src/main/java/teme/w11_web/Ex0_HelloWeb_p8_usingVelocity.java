package teme.w11_web;

import spark.Request;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.post;
import static teme.w11_web.SparkUtil.render;

public class Ex0_HelloWeb_p8_usingVelocity {

    public static void main(String[] args) {

        get("/hello8", (req, res) -> handleGetRequest());
        post("/hello8", (req, res) -> handlePostRequestFromForm(req));

        System.out.println("Server started: http://localhost:4567/hello8");
    }

    private static String handleGetRequest() {
        return formPage("", "");
    }

    private static String handlePostRequestFromForm(Request request) {
        String userName = request.queryParams("username"); //sent by form submit

        return valid(userName) ?
                greetingPage(userName) :
                formPage("Invalid name, please retry!", userName);
    }

    private static String formPage(String errorMessage, String defaultName) {
        Map<String, Object> model = new HashMap<>();
        model.put("defaultName", defaultName);
        model.put("errorMsg", errorMessage);
        return render(model, "ask_name.vm");
    }

    private static String greetingPage(String userName) {
        Map<String, Object> model = new HashMap<>();
        model.put("name", userName);
        return render(model, "greeting.vm");
    }

    private static boolean valid(String userName) {
        return userName != null && !userName.isEmpty() &&
                userName.chars().allMatch(c -> Character.isLetter(c) || Character.isWhitespace(c));
    }
}
