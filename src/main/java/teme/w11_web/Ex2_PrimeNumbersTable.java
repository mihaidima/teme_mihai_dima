package teme.w11_web;

import spark.Request;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;
import static spark.Spark.get;
import static spark.Spark.post;


public class Ex2_PrimeNumbersTable {
    public static void main(String[] args) {
        get("/Prime", (req, res) -> Prime_pageWithForm());
        post("/Prime", (req, res) -> Prime_handlingPostRequestFromForm(req));

        System.out.println("Server started, see:\n" +
                "  http://localhost:4567/Prime\n");
    }

    private static String Prime_pageWithForm() {
        return "<h1>Hello!</h1>" +
                "<form method='post'>" +
                "Enter the lower limit: " +
                "<input type='number' name='lower' value='1'></br>" +
                "Enter the upper limit: " +
                "<input type='number' name='upper' value='1000'></br>" +
                "<input type='submit' value='Submit'>" +
                "</form>";
    }

    private static String Prime_handlingPostRequestFromForm(Request request) {
        long lower = Long.parseLong(request.queryParams("lower"));
        long upper = Long.parseLong(request.queryParams("upper"));

        List<Integer> primes = new ArrayList<>();
        for (int i = (int) lower; i <= upper; i++) {
            if (isPrime(i)) {
                primes.add(i);
            }
        }

        StringBuilder htmlCode = new StringBuilder("<h1> Hello! </h1>" +
                "Lower:" + lower + "<br>" +
                "Upper:" + upper + "<br>" +
                "Needed time: " + System.currentTimeMillis() + " millis <br>" +
                "Total prime numbers: " + primes.size() + "<br> <br>" +
                "<a href='/Prime'> Go back </a> <br> </br>" +
                "<style>" +
                "table, th, td {border: 1px solid black;}" +
                "</style> </head> <body> <table> <tr>");


        List<String> cellFormat = new ArrayList<>(primes.size());
        int count = 0;
        for (Integer prime : primes) {
            if (count != 20) {
                cellFormat.add("<td>" + prime.toString() + "</td>");
                count++;
            } else {
                cellFormat.add("<td>" + prime.toString() + "</td> </tr> <tr>");
                count = 0;
            }
        }

        for (String s : cellFormat) {
            htmlCode.append(s);
        }

        htmlCode.append("</br>");

        return htmlCode.toString();
    }

    static boolean isPrime(int n) {
        if (n == 1 || n == 0) {
            return false;
        }
        if (n == 2) {
            return true;

        } else {
            for (int i = 2; i < sqrt(n); i++) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
