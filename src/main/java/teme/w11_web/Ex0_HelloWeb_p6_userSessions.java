package teme.w11_web;

import spark.Request;
import spark.Response;
import spark.Session;

import java.util.Date;

import static spark.Spark.get;
import static spark.Spark.post;

public class Ex0_HelloWeb_p6_userSessions {
    public static void main(String[] args) {

        get("/hello6", (req, res) -> handleGetRequest(req));
        post("/hello6", (req, res) -> handlePostRequestFromForm(req));

        get("/hello6/reset", (req, res) -> handleResetRequest(req, res));

        System.out.println("Server started: http://localhost:4567/hello6");
    }

    private static String handleGetRequest(Request request) {
        String nameFromUserSession = request.session().attribute("username");

        return nameFromUserSession == null ?
                formPage(request) :
                greetingPage(nameFromUserSession, request.session());
    }

    private static String handlePostRequestFromForm(Request request) {
        String userNameFromForm = request.queryParams("username"); //read name sent by form submit

        request.session().attribute("username", userNameFromForm); //save also user name to his permanent session on server!
        request.session().maxInactiveInterval(15); //make session auto-expire after 15 sec

        return greetingPage(userNameFromForm, request.session());
    }

    private static String formPage(Request request) {
        return "<h1>Hello, stranger.</h1>" +
                "<form method='post'>" +
                "Enter your name: " +
                "<input type='text' name='username'>" +
                "<input type='submit' value='Submit'>" +
                "</form>" +
                "<br>" + showSessionInfo(request.session());
    }

    private static String greetingPage(String userName, Session session) {
        return "<h1> Hello, " + userName + "! </h1>" +
                "<a href='/hello6/reset'> Forget me </a> <br>" +
                "<a href='https://www.google.com/search?q=" + userName + "'> Find yourself on google?</a>" +
                "<br>" + showSessionInfo(session);
    }

    private static String showSessionInfo(Session session) {
        return "<br> User session info:<br>" +
                "<ul>" +
                "<li>id: " + session.id() + "<br>" +
                "<li>creationTime: " + new Date(session.creationTime()) + "<br>" +
                "<li>lastAccess: " + new Date(session.lastAccessedTime()) + "<br>" +
                "<li>expires in: " + session.maxInactiveInterval() + " sec <br>" +
                "<li>keys stored: " + session.attributes() + "<br>" +
                "</ul><br>";
    }

    private static Object handleResetRequest(Request req, Response res) {
        //reset session (remove name)
        req.session().removeAttribute("username");

        //then behave like main page
        res.redirect("/hello6");
        return res;
    }
}
