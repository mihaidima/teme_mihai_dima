package teme.w11_web;

import spark.Request;

import static spark.Spark.get;
import static spark.Spark.post;

public class Ex0_HelloWeb_p5_usingForms {

    public static void main(String[] args) {

        get("/hello5", (req, res) -> handleGetRequest());
        post("/hello5", (req, res) -> handlePostRequestFromForm(req));

        System.out.println("Server started: http://localhost:4567/hello5");
    }

    private static String handleGetRequest() {
        return "<h1>Hello, stranger.</h1>" +
                "<form method='post'>" +
                "Enter your name: " +
                "<input type='text' name='username' value='' autofocus>" +
                "<input type='submit' value='Submit'>" +
                "</form>";
    }

    private static String handlePostRequestFromForm(Request request) {
        String userName = request.queryParams("username"); //sent by form submit

        return "<h1> Hello, " + userName + "! </h1>" +
                "<a href='/hello5'> Go back </a> <br>" +
                "<a href='https://www.google.com/search?q=" + userName + "'> Find yourself on google?</a>";
    }
}
