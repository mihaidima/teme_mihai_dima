package teme.w11_web;

import spark.Request;

import static spark.Spark.get;
import static spark.Spark.post;

public class Ex0_HelloWeb_p7_withValidations {

    public static void main(String[] args) {

        get("/hello7", (req, res) -> handleGetRequest());
        post("/hello7", (req, res) -> handlePostRequestFromForm(req));

        System.out.println("Server started: http://localhost:4567/hello7");
    }

    private static String handleGetRequest() {
        return formPage("", "");
    }

    private static String handlePostRequestFromForm(Request request) {
        String userName = request.queryParams("username"); //sent by form submit

        return valid(userName) ?
                greetingPage(userName) :
                formPage("Invalid name, please retry!", userName);
    }

    private static String formPage(String errorMessage, String defaultName) {
        return "<h1>Hello, stranger.</h1>" +
                "<h2 style='color:red;'>" + errorMessage + "</h2>" +
                "<form method='post'>" +
                "Enter your name: " +
                "<input type='text' name='username' value='" + defaultName + "' autofocus>" +
                "<input type='submit' value='Submit'>" +
                "</form>";
    }

    private static String greetingPage(String userName) {
        return "<h1> Hello, " + userName + "! </h1>" +
                "<a href='/hello7'> Go back </a> <br>" +
                "<a href='https://www.google.com/search?q=" + userName + "'> Find yourself on google?</a>";
    }

    private static boolean valid(String userName) {
        return userName != null && !userName.isEmpty() &&
                userName.chars().allMatch(c -> Character.isLetter(c) || Character.isWhitespace(c));
    }
}
