package teme.w06_collections.ex3_person_registry;

import java.util.HashSet;
import java.util.Set;

class PersonsRegistryImpl implements PersonsRegistry {
    private Set<Person> personRegistry = new HashSet<>();

    @Override
    public String toString() {
        return "PersonsRegistryImpl{" +
                "personRegistry=" + personRegistry +
                '}';
    }

    @Override
    //Adds a new person to the registry. If a person with same CNP already exists,
    //it will NOT register this new person (just ignore it, and show an error)
    public void register(Person p) {
        int initSize = personRegistry.size();
        personRegistry.add(p);
        if ((personRegistry.size() == initSize)) {
            System.out.println("CNP is already registered!");
        }
    }

    @Override
    //Finds a person by cnp and removes it from registry
    //If person not found, will still work (does nothing, but raises no error either)
    public void unregister(int cnp) {
        Set<Person> setTransfer = new HashSet<>();
        for (Person p : personRegistry) {
            if (p.getCnp() != cnp) {
                setTransfer.add(p);
            }
        }
        if (setTransfer.size() != personRegistry.size()) {
            personRegistry.clear();
            personRegistry = setTransfer;
        }
    }

    @Override
    //Get the number of currently registered persons
    public int count() {
        return personRegistry.size();
    }

    @Override
    //Get the list of cnp values of all persons
    public Set<Integer> cnps() {
        Set<Integer> cnps = new HashSet<>();
        for (Person p : personRegistry) {
            cnps.add(p.getCnp());
        }
        return cnps;
    }

    @Override
    //Get the list of unique names of all persons
    public Set<String> uniqueNames() {
        Set<String> uniqueNames = new HashSet<>();
        for (Person p : personRegistry) {
            uniqueNames.add(p.getName());
        }
        return uniqueNames;
    }

    @Override
    //Find a person by cnp. Returns null if no person found.
    public Person findByCnp(int cnp) {
        for (Person p : personRegistry) {
            if (p.getCnp() == cnp) {
                return p;
            }
        }
        return null;
    }

    @Override
    //Find the persons with a specified name (may be zero, one or more)
    //(comparing person name with specified name should be case insensitive)
    public Set<Person> findByName(String name) {
        Set<Person> findByName = new HashSet<>();
        for (Person p : personRegistry) {
            if (p.getName().equalsIgnoreCase(name)) {
                findByName.add(p);
            }

        }
        return findByName;
    }

    @Override
    //Get the average age for all persons (or 0 as default if it cannot be computed)
    public double averageAge() {
        double avrAge = 0;
        for (Person p : personRegistry) {
            avrAge += p.getAge();
        }
        return (personRegistry.size() > 0) ? avrAge / personRegistry.size() : 0;
    }

    @Override
    //Get the percent (a value between 0-100) of adults (persons with age >=18)
    //from the number of all persons (or 0 as default if it cannot be computed)
    public double adultsPercentage() {
        double count = 0;
        for (Person p : personRegistry) {
            if (p.getAge() >= 18) {
                count++;
            }
        }
        return (count > 0) ? ((count * 100) / personRegistry.size()) : 0;
    }

    @Override
    //Get the percent (a value between 0-100) of adults who voted
    //from the number of all adult persons (age>=18)
    public double adultsWhoVotedPercentage() {
        double countHasVoted = 0;
        double totalAdults = 0;
        for (Person p : personRegistry) {
            if (p.isHasVoted() == true) {
                countHasVoted++;
            }
            if (p.getAge() >= 18) {
                totalAdults++;
            }
        }
        return (totalAdults > 0) ? ((countHasVoted * 100) / totalAdults) : 0;
    }

}


