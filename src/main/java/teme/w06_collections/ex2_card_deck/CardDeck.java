package teme.w06_collections.ex2_card_deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class CardDeck {
    List<Card> cardDeck = new ArrayList<>(52);
    List<Card> dealtCards = new ArrayList<>(52);

    CardDeck() {
        cardDeck.clear();
        for (Suit suit : Suit.values()) {
            for (int i = 2; i <= 14; i++) {
                cardDeck.add(new Card(i, suit));
            }
        }
    }

    void shuffle() {
        cardDeck.clear();
        new CardDeck();
        dealtCards.clear();
        Collections.shuffle(cardDeck);
    }

    List<Card> dealHand(int cards) {
        List<Card> dealHand = new ArrayList<>();
        Collections.shuffle(cardDeck);
        for (int i = 0; i < cards; i++) {
            if (cardDeck.isEmpty())
                return dealHand;
            dealHand.add(cardDeck.get(0));
            dealtCards.add(cardDeck.get(0));
            cardDeck.remove(0);
        }
        return dealHand;
    }

    int getAvailableCardCount() {
        return cardDeck.size();
    }

    int getUsedCardCount() {
        return dealtCards.size();
    }


    /**
     * Just for some manual tests
     */
    public static void main(String[] args) {
        CardDeck deck = new CardDeck();
        System.out.println(deck.dealHand(5)); // <- print 5 cards 3 times
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(50)); // <- only 39 cards should be printed here
        System.out.println(deck.dealHand(50)); // <- and empty list should be printed
        deck.shuffle();
        System.out.println(deck.dealHand(5)); // <- another 5 cards
    }
}
