package teme.w06_collections.ex0_word_count;

import java.util.*;

class CountingWords {

    //some manual tests
    public static void main(String[] args) {

        String text = "Once upon a time in a land far far away there lived a great king whose name was a great mystery";
        System.out.println("initial text: " + text);

        System.out.println("\nall words (initial order): " + words(text));
        System.out.println("word count: " + wordsCount(text));
        System.out.println("all words (sorted): " + sortedWords(text));
        System.out.println("distinct words (initial order): " + distinctWords(text));
        System.out.println("distinct words (sorted): " + distinctSortedWords(text));

        System.out.println("\nword counts (initial order): " + wordsUsageCount(text));
        System.out.println("word counts (sorted by word): " + wordsUsageCountSortedByWord(text));
        System.out.println("word counts (sorted by count, then word): " + wordsUsageCountSortedByCountThenWord(text));
    }

    static List<String> words(String text) {
        if (text.isEmpty()) {
            return new ArrayList<String>();
        }
        String[] wordsArr = text.split("\\s+");
        return Arrays.asList(wordsArr);
    }

    static int wordsCount(String text) {
        List<String> list = words(text);
        return list.size();
    }

    static Collection<String> sortedWords(String text) {
        List<String> list = words(text);
        Collections.sort(list);
        return list;
    }

    static Collection<String> distinctWords(String text) {
        List<String> list = words(text);
        /*Set<String> distinctWords = new LinkedHashSet<>();
        distinctWords.addAll(list);*/
        return new LinkedHashSet<>(list);
    }

    static Collection<String> distinctSortedWords(String text) {
        List<String> list = words(text);
        return new TreeSet<>(list);
    }

    static Map<String, Long> wordsUsageCount(String text) {
        return null;
    }

    static Map<String, Long> wordsUsageCountSortedByWord(String text) {
        return null; //TODO
    }

    static List<Map.Entry<String, Long>> wordsUsageCountSortedByCountThenWord(String text) {
        return null; //TODO
    }
}
