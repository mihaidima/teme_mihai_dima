package teme.w07_comparable.ex3_palindrome;

import java.util.Arrays;

public class Palindrome {

    static boolean isSymmetrical(int[] array) {
        //TODO
        return false;
    }

    //TODO: make this generic version also work (similar to previous one, but should work with ANY type of object, not just int values)
    //after this, also uncomment the JUnit test for this method!
    /*
    static ?? boolean isSymmetrical_generic( ?? [] array) {
        return false;
    }
    */


    static boolean isPalindrome(String text) {
        //TODO
        return false;
    }


    public static void main(String[] args) {

        testSym(new int[]{1, 2, 3, 4, 1});

        testSym(new int[]{1});
        testSym(new int[]{2, 2});
        testSym(new int[]{1, 2, 3, 2, 1});


        //TODO: uncomment after making generic version work
        /*
        testSym_generic(new String[]{"ab", "cd"});
        testSym_generic(new String[]{"ab", "cd", "cd", "ab"});
        testSym_generic(new String[]{"ab", "cd", "cd", "ef", "ab"});
        testSym_generic(new Boolean[]{true, false, true});
        */


        testPal("not a palindrome");
        testPal("abb");
        testPal("abcXa");

        testPal("a");
        testPal("abba");
        testPal("rotitor");
        testPal("Step on no pets");
        testPal("Ene purta patru pene");
        testPal("Ion a luat luni vinul tau la noi");
    }

    private static void testSym(int[] array) {
        System.out.println("array: " + Arrays.toString(array) + " - is symmetrical?: " + isSymmetrical(array));
    }

    /*
    private static void testSym_generic(Object[] array) {
        System.out.println("array: " + Arrays.toString(array) + " - is symmetrical?: " + isSymmetrical_generic(array));
    }
    */

    private static void testPal(String text) {
        System.out.println("'" + text + "' - is palindrome?: " + isPalindrome(text));
    }
}
