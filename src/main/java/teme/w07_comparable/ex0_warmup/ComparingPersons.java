package teme.w07_comparable.ex0_warmup;

import java.util.*;

public class ComparingPersons {

    public static void main(String[] args) {


        List<Person> personList = Arrays.asList(
                new Person(1111, "Ion", 20, 180),
                new Person(3333, "Ana", 22, 170),
                new Person(2222, "Vasile", 22, 170),
                new Person(4444, "Maria", 19, 160));

        Set<Person> persons = new TreeSet<>(personList);
        System.out.println("\nPersons (default sorting):");
        for (Person p : persons) System.out.println(p);

        Set<Person> sortedByCnp = new TreeSet<>(new PersonComparatorByCnp());
        sortedByCnp.addAll(personList);
        System.out.println("\nPersons (sorted by cnp):");
        for (Person p : sortedByCnp) System.out.println(p);

        /*//TreeSet+Comparator => contains only 3 persons, not 4!! persons for which comparator returns 0 are considered duplicates by a TreeSet!!
        Set<Person> sortedByHeightAndAge = new TreeSet<>(new PersonComparatorByHeightAndAge());
        sortedByHeightAndAge.addAll(personList);
        System.out.println("\nPersons (sorted by height+age, with a TreeSet):");
        for (Person p : sortedByHeightAndAge) System.out.println(p);

        //but if we use a List with .sort(), we will retain all 4 persons, including the ones for which comparator returns 0 (unlike for the TreeSet case)
        List<Person> listSortedByHeightAndAge = new ArrayList<>(personList);
        listSortedByHeightAndAge.sort(new PersonComparatorByHeightAndAge());
        System.out.println("\nPersons (sorted by height+age, in a List):");
        for (Person p : listSortedByHeightAndAge) System.out.println(p);
        */
    }
}

class Person implements Comparable<Person> {
    private long cnp;
    private String name;
    private int age;
    private int height;

    public Person(long cnp, String name, int age, int height) {
        this.cnp = cnp;
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public long getCnp() {
        return cnp;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }


    @Override
    public String toString() {
        return "Person{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                '}';
    }


    @Override
    public int compareTo(Person person) {
        return this.name.compareTo(person.name);
    }
}

class PersonComparatorByCnp implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        /*if (p1.getCnp()<p2.getCnp()){
            return -1;
        }else{
           if (p1.getCnp()>p2.getCnp()){
                return 1;
            }
        }
        return 0;*/
        return Long.compare(p1.getCnp(), p2.getCnp());
    }
}

class PersonComparatorByHeightAndAge {

}
