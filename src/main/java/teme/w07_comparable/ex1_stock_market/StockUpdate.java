package teme.w07_comparable.ex1_stock_market;

import java.time.LocalDate;
import java.util.Objects;

class StockUpdate implements Comparable<StockUpdate> {
    private LocalDate lastUpdate;
    private String code;
    private double price;


    StockUpdate(String code, LocalDate lastUpdate, double price) {
        this.lastUpdate = lastUpdate;
        this.code = code.toUpperCase();
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "StockUpdate{" +
                "code='" + code + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockUpdate that = (StockUpdate) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(code, that.code) &&
                Objects.equals(lastUpdate, that.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, lastUpdate, price);
    }

    @Override
    public int compareTo(StockUpdate stockUpdate) {
        return this.lastUpdate.compareTo(stockUpdate.lastUpdate);
    }

}
