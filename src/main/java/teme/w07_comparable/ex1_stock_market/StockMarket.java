package teme.w07_comparable.ex1_stock_market;

import java.time.LocalDate;
import java.util.*;


class StockMarket implements Comparator<StockUpdate> {
    private Set<StockUpdate> stock = new TreeSet<>();


    void add(StockUpdate update) {
        stock.add(update);
    }

    List<StockUpdate> getUpdates(LocalDate from, LocalDate to) {
        Set<StockUpdate> dataTrans = new TreeSet<>(stock);
        for (Iterator<StockUpdate> iterator = dataTrans.iterator(); iterator.hasNext(); ) {
            StockUpdate s = iterator.next();
            if (from.compareTo(s.getLastUpdate()) == 1 || to.compareTo(s.getLastUpdate()) == -1) {
                dataTrans.remove(s);
            }
        }
        List<StockUpdate> result = new ArrayList<>();
        result.addAll(dataTrans);
        return result;
    }

    List<StockUpdate> getUpdates(LocalDate from, LocalDate to, String code) {
        Set<StockUpdate> dataTrans = new TreeSet<>(stock);
        for (Iterator<StockUpdate> iterator = dataTrans.iterator(); iterator.hasNext(); ) {
            StockUpdate s = iterator.next();
            if (!code.toUpperCase().equals(s.getCode())) {
                dataTrans.remove(s);
            }
        }
        for (Iterator<StockUpdate> iterator = dataTrans.iterator(); iterator.hasNext(); ) {
            StockUpdate s = iterator.next();
            if (from.compareTo(s.getLastUpdate()) == 1 || to.compareTo(s.getLastUpdate()) == -1) {
                dataTrans.remove(s);
            }
        }
        List<StockUpdate> result = new ArrayList<>();
        result.addAll(dataTrans);
        return result;
    }

    Map<String, Double> getPrices(LocalDate date) {
/*        Set<StockUpdate> dataTrans = new TreeSet<>();
        dataTrans.addAll(stock);
        for (StockUpdate s : dataTrans) {
            if (!code.toUpperCase().equals(s.getCode())) {
                dataTrans.remove(s);
            }
        }
        for (StockUpdate s : dataTrans) {
            if (from.compareTo(s.getLastUpdate()) == 1 || to.compareTo(s.getLastUpdate()) == -1) {
                dataTrans.remove(s);
            }
        }*/
        return null;
    }

    double getPrice(LocalDate date, String code) {
        Set<StockUpdate> dataTrans = new TreeSet<>(stock);
        for (StockUpdate s : dataTrans) {
            if (!code.toUpperCase().equals(s.getCode())) {
                dataTrans.remove(s);
            }
        }
        for (StockUpdate s : dataTrans) {
            if (date.compareTo(s.getLastUpdate()) == 1) {
                dataTrans.remove(s);
            }
        }
        double result = -1;
        for (Iterator<StockUpdate> iterator = dataTrans.iterator(); iterator.hasNext(); ) {
            StockUpdate s = iterator.next();
            result = s.getPrice();
        }
        return result;
    }


    /*double getPrice1(LocalDate date, String code) {
        Set<StockUpdate> dataTrans = new TreeSet<>();
        dataTrans.addAll(stock);
        for (StockUpdate s : dataTrans) {
            if (!code.toUpperCase().equals(s.getCode())) {
                dataTrans.remove(s);
            }
        }
        return -1;
    }*/

    @Override
    public int compare(StockUpdate s1, StockUpdate s2) {
        if (s1.getCode().compareTo(s2.getCode()) == -1) {
            return -1;
        } else {
            if (s1.getCode().compareTo(s2.getCode()) == 1) {
                return 1;
            }
        }
        return 0;
    }
}
