package teme.w10_recap;

public class Ex0b_CheckPrime {

    public static boolean isPrime(int n) {
        if (n < 0) {
            throw new InvalidNumberException("primes don't apply to negative numbers!");
        }
        if (n == 1 || n == 0) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}

class InvalidNumberException extends RuntimeException {
    InvalidNumberException(String errorMsg) {
        super(errorMsg);
    }
}

