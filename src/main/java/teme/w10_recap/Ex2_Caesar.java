package teme.w10_recap;

public class Ex2_Caesar {
    public static void main(String[] args) {

        String text = "A fost odata?..";
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            char next = (char) (c + 5);
            System.out.println("Original: " + c + " ->  original+5: " + next);
        }
    }
}
