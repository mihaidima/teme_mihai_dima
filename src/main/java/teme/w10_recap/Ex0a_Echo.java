package teme.w10_recap;

import java.util.Scanner;

public class Ex0a_Echo {
    public static void main(String[] args) {
        echo();
    }

    private static void echo() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Entered echo room:");

        String line = sc.nextLine();
        while (!line.equals("exit")) {
            System.out.println(line.toUpperCase() + "... " + line.toLowerCase() + "...");
            line = sc.next();
        }
    }
}
