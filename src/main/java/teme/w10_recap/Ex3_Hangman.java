package teme.w10_recap;

import java.util.Scanner;

public class Ex3_Hangman {

    public static void main(String[] args) {

        String storedWord;
        char[] charString;
        int length;
        char[] censor;
        int attempts = 0;

        StringBuilder pastGuesses = new StringBuilder();

        Scanner typedWord = new Scanner(System.in);
        System.out.println("Enter your word to guess: ");
        storedWord = typedWord.nextLine();
        storedWord = storedWord.toUpperCase();
        length = storedWord.length();

        charString = storedWord.toCharArray();

        censor = storedWord.toCharArray();
        System.out.println("Your secret word is: ");

        for (int index = 0; index < length; index++) {
            censor[index] = '*';
        }

        //Main loop to take guesses (is this while loop the ideal loop here?
        while (!String.valueOf(censor).equals(storedWord)) {

            //Initialize all variables in loop
            char charGuess;
            String tempword;
            String tempString;
            boolean correct = false;
            int times = 0; //number of times a letter is in the word
            boolean repeated = false;

            //prints the censored secret word
            for (int a = 0; a < length; a++) {
                System.out.print(censor[a]);
            }
            System.out.println();

            //asks user for guess, then stores guess in Char charGuess and String tempString
            Scanner guess = new Scanner(System.in);
            System.out.println("Type your guess: ");
            tempword = guess.next();
            charGuess = tempword.charAt(0);

            pastGuesses.append(charGuess);
            tempString = pastGuesses.toString();

            //checks if user already guessed the letter previously
            if (tempString.lastIndexOf(charGuess, tempString.length() - 2) != -1) {
                System.out.println("You already guessed this letter! Guess again. Your previous guesses were: ");
                pastGuesses.deleteCharAt(tempString.length() - 1);
                System.out.println(tempString.substring(0, tempString.length() - 1));
                repeated = true;
            }

            //if the guess is not a duplicated guess, checks if the guessed letter is in the word
            if (!repeated) {
                for (int index = 0; index < length; index++) {

                    if (charString[index] == Character.toUpperCase(charGuess)) {

                        censor[index] = Character.toUpperCase(charGuess);  //replaces * with guessed letter in caps
                        correct = true;
                        times++;
                    }
                }
                if (correct) {
                    System.out.println("The letter " + charGuess + " is in the secret word! There are " + times + " " + charGuess + " 's in the word. Revealing the letter(s): ");
                } else {
                    System.out.println("Sorry, the letter is not in the word. Your secret word:  ");
                }
                System.out.println();
            }
            attempts++;
        }
        System.out.println("You guessed the entire word " + storedWord.toUpperCase() + " correctly! It took you " + attempts + " attempts!");
    }

    //how to implement image
     /*   public static void hangmanImage() {
            if (count == 1) {
                System.out.println("Wrong guess, try again");
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println("___|___");
                System.out.println();
            }
            if (count == 2) {
                System.out.println("Wrong guess, try again");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 3) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   | ");
                System.out.println("___|___");
            }
            if (count == 4) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 5) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |           |");
                System.out.println("   |           |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 6) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |           |");
                System.out.println("   |           |");
                System.out.println("   |          / \\ ");
                System.out.println("___|___      /   \\");
            }
            if (count == 7) {
                System.out.println("GAME OVER!");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |          _|_");
                System.out.println("   |         / | \\");
                System.out.println("   |          / \\ ");
                System.out.println("___|___      /   \\");
                System.out.println("GAME OVER! The word was " + word);
            }
        }

    }*/
}
