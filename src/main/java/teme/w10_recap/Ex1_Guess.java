package teme.w10_recap;

import java.util.Random;
import java.util.Scanner;

public class Ex1_Guess {
    public static void main(String[] args) {
        int secret = 1 + new Random().nextInt(100); //1..100

        Scanner sc = new Scanner(System.in);
        int attemptsLeft = 7;

        while (attemptsLeft > 0) {
            System.out.println("Enter your guess (1..100):");
            int guess = sc.nextInt(); //TODO: should handle exceptions here if not entering a number

            if (guess == secret) {
                System.out.println("Bravo! you won!");
                break;
            } else if (guess < secret) {
                System.out.println("Wrong guess, aim HIGHER!");
            } else { //guess > secret
                System.out.println("Wrong guess, aim LOWER!");
            }

            attemptsLeft--;
            System.out.println("Attempts left: " + attemptsLeft);
        }

        if (attemptsLeft == 0) {
            System.out.println("Game over, you lost, number was: " + secret);
        }
    }
}

