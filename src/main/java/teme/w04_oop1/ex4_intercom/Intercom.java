package teme.w04_oop1.ex4_intercom;

class Apartament {
    private int apNum;
    private String apOwner;
    private String code;

    public Apartament(int apNum, String apOwner, String code) {
        this.apNum = apNum;
        this.apOwner = apOwner;
        this.code = code;
    }

    public int getApNum() {
        return apNum;
    }

    public String getApOwner() {
        return apOwner;
    }

    public String getCode() {
        return code;
    }

    public void setApOwner(String apOwner) {
        this.apOwner = apOwner;
    }
}


public class Intercom {

    private int totalApartments;
    private Apartament[] intercomList;

    public Intercom(int totalApartments) {
        this.totalApartments = totalApartments;
        this.intercomList = new Apartament[this.totalApartments];
    }

    //getters & setters
    public int getTotalApartments() {
        return totalApartments;
    }


    //Methodes
    private int getCount() {
        int count = 0;
        for (Apartament ap : intercomList) {
            if (ap != null) {
                count++;
            }
        }
        return count;
    }

    /*adding a new owner to Intercom list
    Necessary to add all apartments and owner as the Intercom as apartment numeber can vary from building to building
    */
    public void addNewAp(Apartament ap) {
        if (getCount() < getTotalApartments()) {
            int i = 0;
            while (i < getCount()) {
                if (ap.getApOwner().equals(intercomList[i].getApOwner())) {
                    System.out.println("Owner is already in the list");
                    return;
                }
                i++;
            }
            intercomList[i] = ap;
        }
    }

    /*adding an new owner to the apartment in case the apartment is sold
        -is a simple way to update the list of owners with access from a terminal
    */
    public void setNewOwners(String apOwner) {
        for (Apartament ap : intercomList) {
            if (ap.getApOwner().equals(apOwner)) {
                ap.setApOwner(apOwner);
            }
        }
    }

    //needed for card access and Code access. Card contains the the information of apNumber and apOwner as a bar code
    public boolean accessCardAndCode(String barCode) {
        /*
       A boolean function that check if the barCode from the card is correct and will open the door
         */
        for (Apartament apCode : intercomList) {
            if (barCode.equals(apCode.getCode())) {
                return true;
            }
        }
        return false;
    }


    /*needed for calling
    user insert the apartment number and will call the apartment
    */
    private boolean calling(int apNum) {
        for (Apartament call : intercomList) {
            if (call.getApNum() == apNum) {
                System.out.println("Calling");
                return true;
            }
        }
        return false;
    }

    //needed for granting access from the apartment
    public boolean grantingAccess(int apNum) {
        /*
        The owner will have 3 options:
            1. Open by pressing the OPEN key from the Intercom keyboard
            2. Refuse to open by pressing the Close key from Intercom keyboard
            3. Is not home and the function will stop after a period of time.
         */
        //better approach to use Case instead of If

        if (calling(apNum)) {
            String sc = System.console().readLine();
            return sc.equals("Open");
        }
        return false;
    }

}
