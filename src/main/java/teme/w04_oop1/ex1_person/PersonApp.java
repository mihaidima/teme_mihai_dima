package teme.w04_oop1.ex1_person;

public class PersonApp {

    //just for manual testing of the Person class
    public static void main(String[] args) {

        Person p1 = new Person("Ion", 1989, "black");
        Person p2 = new Person("Ana", 2001, "blond");
        System.out.println("p2:" + p2.toString());
        Person p3 = new Person("Ana", 2020);
        System.out.println("p1: " + p1.toString());
        System.out.println("p3: " + p3.toString());
        System.out.println("p1 older than p2: " + p1.isOlderThan(p2));
        System.out.println("p3 : " + p3);
        System.out.println(p3.getAgeInYear(2053));

    }
}


