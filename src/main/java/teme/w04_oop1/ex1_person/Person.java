package teme.w04_oop1.ex1_person;

public class Person {
    private String name;
    private int birthYear;
    private String hairColor;


    //contructors

    public Person(String name, int birthYear, String hairColor) {
        this.name = name;
        this.birthYear = birthYear;
        this.hairColor = hairColor.equals("") ? "brown" : hairColor;
    }

    public Person(String name, int birthYear) {
        this(name, birthYear, "");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", birthYear=" + birthYear +
                ", hairColor='" + hairColor + '\'' +
                '}';
    }

    //getters
    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getHairColor() {
        return hairColor;
    }

    //setters
    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    //methode


    public boolean isOlderThan(Person other) {
        return this.getBirthYear() > other.getBirthYear();
    }

    public int getAgeInYear(int year) {
        return (this.getBirthYear() <= year) ? year - this.getBirthYear() : 0;
    }

}

