package teme.w04_oop1.ex2_room;

import teme.w04_oop1.ex1_person.Person;


public class RoomApp {
    //just for manual testing of the Room class

    public static void main(String[] args) {
        Room room = new Room(10);
        new Person("Ion", 1997, "blond");
        room.printAll();
        //...
    }
}


//(using in it the existing Person class, done for previous exercise and placed in separate package)


class Room {
    final int capacity;
    Person[] allPersons;

    Room(int capacity) {
        this.capacity = capacity;
        this.allPersons = new Person[capacity];
    }

    int getCount() {
        int count = 0;
        for (Person allPerson : allPersons) {
            if (allPerson != null) {
                count++;
            }
        }
        return count;
    }

    void printAll() {
        System.out.println(getCapacity());
        System.out.println(getCount());
        for (Person person : allPersons) {
            System.out.println(person);
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public void enter(Person person) {
        if (getCount() < getCapacity()) {
            int i = 0;
            while (i < getCount()) {
                if (person.getName().equals(allPersons[i].getName())) {
                    System.out.println("Person is already in the room");
                    return;
                }
                i++;
            }
            allPersons[i] = person;
        }
    }

    String getOldest() {
        Person oldestPers = allPersons[0];
        if (getCount() > 0) {
            for (int i = 0; i < getCount(); i++) {
                if (oldestPers.getBirthYear() >= allPersons[i].getBirthYear()) {
                    oldestPers = allPersons[i];
                    System.out.println(oldestPers);
                }
            }
            return oldestPers.getName() + "(" + oldestPers.getBirthYear() + ")";
        }
        return "";
    }

    String[] getNames(String hairColor) {
        int j = 0;
        for (int i = 0; i < getCount(); i++) {
            if (allPersons[i].getHairColor().equals(hairColor)) {
                j++;
            }
        }
        String[] personWithSameHairColor = new String[j];
        j = 0;
        for (int i = 0; i < getCount(); i++) {
            if (allPersons[i].getHairColor().equals(hairColor)) {
                personWithSameHairColor[j] = allPersons[i].getName();
                j++;
            }
        }
        return personWithSameHairColor;
    }

    boolean isPresent(String personName) {
        return indexOf(personName) != -1;
    }

    private int indexOf(String personName) {
        for (int i = 0; i < getCount(); i++) {
            if (allPersons[i].getName().equals(personName)) {
                return i;
            }
        }
        return -1;
    }

    void exit(String personName) {
        int indexCont = indexOf(personName);
        if (indexCont == -1) {
            return;
        }
        int i = indexCont;
        while (i < getCount() - 1) {
            allPersons[i] = allPersons[i + 1];
            i++;
        }
        allPersons[i] = null;
    }

}
