package teme.w04_oop1.ex5_atm;


class Card {
    //properties
    private int cardNo;
    private String cardOwner;
    private int pinCode;
    private String bankofCard;
    private String accountNumber;

    public Card(int cardNo, String cardOwner, int pinCode, String bankofCard) {
        this.cardNo = cardNo;
        this.cardOwner = cardOwner;
        this.pinCode = pinCode;
        this.bankofCard = bankofCard;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getCardNo() {
        return cardNo;
    }

    public String getCardOwner() {
        return cardOwner;
    }

    public int getPinCode() {
        return pinCode;
    }

    /*
    Method to check if the pin code is correct
     */
    static boolean pinCodeCheck(int pinCode) {
        /*
        Using If function to checked if the pin code introduced by the user is correct
        this will be done by checking the accountNumber array at the pin code position
        also the number of wrong pin code will be retained to lock it.
         */
        return true;
    }
}

class Account {
    private int funds;
    private String accountNumber;
    private int maxAmountLimit;
    private String accountOwner;

    public Account(int funds, String accountNumber, int maxAmountLimit, String accountOwner) {
        this.funds = funds;
        this.accountNumber = accountNumber;
        this.maxAmountLimit = maxAmountLimit;
        this.accountOwner = accountOwner;
    }
}

class Bank {
    private String bank;
    private final int maxAmount;
    private final int maxwithdrawNo;
    private Card[] cardInformation;
    private Account[] accountinfo;

    Bank(String accountNo, String bank, int maxAmount, int maxwithdrawNo) {
        this.bank = bank;
        this.maxAmount = maxAmount;
        this.maxwithdrawNo = maxwithdrawNo;
    }
}


public class Atm {

    //properties
    private String bankOfAtm;
    private int[] withdrawAmount = {10, 50, 100, 150, 200, 300, 400};
    private String[] language = new String[5];
    private int insertValue;
    private int amountOf10 = 1000;
    private int amountOf50 = 1000;
    private int amountOf100 = 500;
    private int amountOf200 = 100;


    public Atm(String bankOfAtm, String[] language) {
        this.bankOfAtm = bankOfAtm;
        this.language = language;
    }

    public Atm(int insertValue) {
        this.insertValue = insertValue;
    }

    public int getAmountOf10() {
        return amountOf10;
    }

    public int getAmountOf50() {
        return amountOf50;
    }

    public int getAmountOf100() {
        return amountOf100;
    }

    public int getAmountOf200() {
        return amountOf200;
    }

    public String getBankOfAtm() {
        return bankOfAtm;
    }

    public void setAmountOf10(int amountOf10) {
        this.amountOf10 = amountOf10;
    }

    public void setAmountOf50(int amountOf50) {
        this.amountOf50 = amountOf50;
    }

    public void setAmountOf100(int amountOf100) {
        this.amountOf100 = amountOf100;
    }

    public void setAmountOf200(int amountOf200) {
        this.amountOf200 = amountOf200;
    }

    //Method


    /*
    initialization of the card by accessing the account number array where all information is stored.
     */
    static boolean initialisation(String[] cardInformation) {
        //will access all the information needed by accessing the card
        //wil allso access the pinCode methode.
        return false;
    }

    /*
    accesing the option of withdraw from ATM
     */
    public static int withdraw(String option) {
        /**
         *the ATM will check the quantity of money and if the available amount of each is out a message will apper:
         * "Use multiple of <the smallest currency from the available amounts>"
         * after it automatically will move to the "Other amount" filed where the use needs to insert a multiple.
         * the result will be the amount that have to be withdraw
         */
        return -1;
    }


    /* method to withdraw the money
     */
    public String makeTheTransactionp(int insertValue) {
        String bank;
        /**first of all will call the "withdraw" method
         * will update the remaining amounts of money from each currency in the ATM by using the setters.
         * */
        return "Thank you bor using" + bankOfAtm;
    }

    /* the method is defined to check if there are sufficient founds in the account.
     * this will be done by checking with the information from account array
     */
    public static String availablefunds() {
        int amount = 0;
        return "availabl;e amount: " + amount;
    }


}
