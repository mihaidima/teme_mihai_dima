package teme.w04_oop1.ex3_point;

import static java.lang.Math.sqrt;

public class PointApp {

    //just for manual testing of the Room class
    public static void main(String[] args) {
        Point p1 = new Point(2, 3);
        System.out.println(p1.distanceTo(new Point(4, 5)));
        //..
    }
}

class Point {
    //if x and y are final the move methode will not function
    private double x;
    private double y;

    //with "null" constructor the points can be created but the values of x and y are "0"
    //the "null" constructor will work after the param constructor is added as long as a new Point will be created with no given values for x and y
    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //getter
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    double distanceTo(Point other) {
        return sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
    }

    void move(double deltaX, double deltaY) {
        this.x = x + deltaX;
        this.y = y + deltaY;
    }

    static boolean canFormTriangle(Point p1, Point p2, Point p3) {
        double side1 = p1.distanceTo(p2);
        double side2 = p2.distanceTo(p3);
        double side3 = p3.distanceTo(p1);
        return (side1 + side2 > side3) &&
                (side1 + side3 > side2) &&
                (side2 + side3 > side1);
    }

    static boolean canFormRightAngledTriangle(Point p1, Point p2, Point p3) {
        return (canFormTriangle(p1, p2, p3)) &&
                (pythagoras(p1, p2, p3) || pythagoras(p3, p1, p2) || pythagoras(p2, p3, p1));
    }

    private static boolean pythagoras(Point p1, Point p2, Point p3) {
        double shortSide1 = p1.distanceTo(p2);
        double shortSide2 = p2.distanceTo(p3);
        double longSide = p3.distanceTo(p1);
        return shortSide1 * shortSide1 + shortSide2 * shortSide2 == longSide * longSide;
    }


    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

