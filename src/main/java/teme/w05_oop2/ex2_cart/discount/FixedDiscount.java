package teme.w05_oop2.ex2_cart.discount;

import teme.w05_oop2.ex2_cart.product.Product;

public class FixedDiscount extends Product implements Discount {

    private double amount;

    public FixedDiscount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public double computePriceAfterDiscount(double price) {
        return amount;
    }

    @Override
    public String toString() {
        return "FixedDiscount{" +
                "amount=" + amount +
                '}';
    }
}
