package teme.w05_oop2.ex2_cart.discount;


import teme.w05_oop2.ex2_cart.product.Product;

public class PercentageDiscount extends Product implements Discount {
    private double percent;

    public PercentageDiscount(double percent) {
        this.percent = percent;
    }

    private double getPercent() {
        return percent;
    }

    @Override
    public double computePriceAfterDiscount(double price) {
        return price * (getPercent() / 100);
    }

    @Override
    public String toString() {
        return "PercentageDiscount{" +
                "percent=" + percent +
                '}';
    }
}
