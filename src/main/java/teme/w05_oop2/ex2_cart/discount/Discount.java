package teme.w05_oop2.ex2_cart.discount;

public interface Discount {

    double computePriceAfterDiscount(double price);


}

