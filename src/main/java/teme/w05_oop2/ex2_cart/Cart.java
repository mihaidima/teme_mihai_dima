package teme.w05_oop2.ex2_cart;

import teme.w05_oop2.ex2_cart.customer.Customer;
import teme.w05_oop2.ex2_cart.discount.Discount;
import teme.w05_oop2.ex2_cart.product.Product;

import java.util.ArrayList;
import java.util.List;


public class Cart {

    private Customer customer;
    private List<Product> allProducts = new ArrayList<>();
    private List<Discount> allDiscounts = new ArrayList<>();

    public Cart(Customer customer) {
        this.customer = customer;
    }

/*    private void setAllProducts(Product[] allProducts) {
        Cart.allProducts = allProducts;
    }

    private void setAllDiscounts(Discount[] allDiscounts) {
        this.allDiscounts = allDiscounts;
    }

    void addProduct(Product product) {
        Product[] addProd;
        if (allProducts.length < 1) {
            addProd = new Product[1];
            addProd[0] = product;
        } else {
            addProd = new Product[allProducts.length + 1];
            addProd = copyOfRange(allProducts, 0, allProducts.length + 1);
            addProd[addProd.length - 1] = product;
        }
        setAllProducts(addProd);
    }


    void addDiscount(Discount discount) {
        if (allDiscounts.length < 1) {
            Discount[] addDisc = new Discount[1];
            addDisc[0] = discount;
            setAllDiscounts(addDisc);
        } else {
            Discount[] addDisc = new Discount[allDiscounts.length + 1];
            addDisc = copyOfRange(allDiscounts, 0, allDiscounts.length+1);
            addDisc[allDiscounts.length] = discount;
            setAllDiscounts(addDisc);
        }
    }

    void removeDiscount(Discount d) {
        if (allDiscounts.length != 0) {
            Discount[] delDisc;
            int count = 0;
            for (Discount disc : allDiscounts) {
                if (!disc.equals(d)) {
                    count++;
                } else {
                    break;
                }
            }
            if ((count != 0) && (count != allDiscounts.length)) {
                delDisc = new Discount[allDiscounts.length - 1];
                delDisc = copyOfRange(allDiscounts, 0, count - 2);
                if (allDiscounts.length - count + 1 >= 0)
                    System.arraycopy(allDiscounts, count + 1, delDisc, count + 1 - 1, allDiscounts.length - count + 1);
                setAllDiscounts(delDisc);
            }
        }
    }
*/

    void addProduct(Product product) {
        allProducts.add(product);
    }

    void removeProduct(Product product) {
        allProducts.remove(product);
    }

    void addDiscount(Discount discount) {
        allDiscounts.add(discount);
    }

    void removeDiscount(Discount discount) {
        allDiscounts.remove(discount);
    }

    double computeProductsPrice() {
        double productPrice = 0;
        for (Product i : allProducts) {
            productPrice = productPrice + i.getPrice();
        }
        return productPrice;
    }

    double computeTotalPrice() {
        double totalPrice = computeProductsPrice();
        double discount;
        for (Discount disc : allDiscounts) {
            discount = disc.computePriceAfterDiscount(totalPrice);
            System.out.println(discount);
            totalPrice -= discount;
        }
        return (totalPrice > 0) ? totalPrice : 0;
    }

    @Override
    public String toString() {
        return "\n" + customer +
                "\n" + allProducts +
                "\n" + allDiscounts +
                '}';
    }

    public String generateInvoice() {
        String invoice;
        return customer.toString() + "\n" + allProducts.toString() + "\n" + computeProductsPrice() + "\n" + allDiscounts.toString() + "\n" + computeTotalPrice() + "\n";
    }

}

/*
interface CartItem {
    String getProduct();

    Double PriceAfterDiscount();
}*/
