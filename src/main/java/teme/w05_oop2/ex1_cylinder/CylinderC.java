package teme.w05_oop2.ex1_cylinder;

/*Version without Interface Cylinder
 *Overall the code written in this class is much more simple than the one from CylinderH
 * the only challenge is that you need to pay attention to the way you call the methods from class Circle
 * advantage:
 *you don't need to write the methods, you just need to use the results
 * disadvantage:
 * */

/*Version with Interface Cylinder:
 *first of all I'm forced to implement the methods: area(), volume(), toString()
 * toString() i've added as this must @Override the Circle class method toString() */

public class CylinderC implements Cylinder {
    private Circle base;
    private double height;

    CylinderC(double centerX, double centerY, double radius, double height) {
        this.base = new Circle(centerX, centerY, radius);
        this.height = height;
    }

    public CylinderC(Circle circle, double height) {
        this(circle.getCenterX(), circle.getCenterY(), circle.getRadius(), height);
    }


    public double getHeight() {
        return height;
    }

    @Override
    public double getRadius() {
        return base.getRadius();
    }

    @Override
    public double area() {
        return Cylinder.super.area();
    }

    public Circle getBase() {
        return new Circle(base.getCenterX(), base.getCenterY(), base.getRadius());
    }

    /*@Override //=> after I have created methods in Cylinder
    public double area(Circle base) {
        return Cylinder.super.area(base);
    }*/

    @Override
    public double volume() {
        return Cylinder.super.volume();
    }

    @Override
    public String toString() {
        return "CylinderC{" +
                "base=" + base +
                ", height=" + height +
                '}';
    }

    public double length() {
        return 2 * Math.PI * base.getRadius();
    }



/*Version without Interface Cylinder
*all the points above are valid also for this version
    double volume() {
        return base.area() * getHeight();
    }


    public double area() {
        return base.area();
    }
    //first time have resulted with error as the method was not created. After I have implement the method no error.
    public double length() {
        return 2 * Math.PI * base.getRadius();
    }


    @Override
    public String toString() {
        return "CylinderC{" +
                "base=" + base +
                ", height=" + height +
                '}';
    }*/
}
