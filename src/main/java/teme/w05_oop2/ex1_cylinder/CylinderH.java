package teme.w05_oop2.ex1_cylinder;

public class CylinderH extends Circle implements Cylinder {

    private double height;

    CylinderH(double centerX, double centerY, double radius, double height) {
        super(centerX, centerY, radius);
        this.height = height;
    }

    public CylinderH(Circle circle, double height) {
        this(circle.getCenterX(), circle.getCenterY(), circle.getRadius(), height);
    }


    @Override
    public double length() {
        return 0;
    }

    public double getHeight() {
        return height;
    }

    public Circle getBase() {
        return new Circle(getCenterX(), getCenterY(), getRadius());
    }

    @Override
    public double area() {
        return 2 * super.area() + getHeight() * super.length();
    }

    @Override
    public double volume() {
        return super.area() * getHeight();
    }

    @Override
    public String toString() {
        return "CylinderH{ Base=" + getBase() +
                ", height=" + height +
                "}";
    }

    /*Version without Interface Cylinder:
     * can be created 2 constructors with the given parameters
     * all the date needed to create the cylinder are available by accessing the getters from class Circle
     * If the fields are settled as Final then the values ca be accessed to get the coordinate values of the center but we will never be able to move it
     * Volume have to be calculated with the super.area()
     *
     *
     *
    double volume() {
        return super.area() * getHeight();
    }



     * The cylinder area have his one formula therefore another area() method must be created
     * One challenge is to know exactly to which area you need to refer
     * because the way you calculate it will influence the Volume as cylinder area is bigger.
     * Best approach is to have a different name for the cylinder area calculation.
     * In my case I have used the same name as i wished to use "super.area()"
     *
    double area() {
        return 2 * super.area() + getHeight() * super.length();
    }


    *
     * Override of toString() can use the Circle class toString() method*//*
    @Override
    public String toString() {
        return "CylinderH{ Base=" + getBase() +
                ", height=" + height +
                '}';
    }*/
}
