package teme.w05_oop2.ex1_cylinder;

class CircleCylinder {

    public static void main(String[] args) {


        Circle base = new Circle(0.1, 0.2, 2.5);
        System.out.println("base: " + base);
        System.out.println("base area: " + base.area() + ", length: " + base.length());


        //with inheritance:
        CylinderH ch = new CylinderH(0.1, 0.2, 2.5, 3.5);
        System.out.println("\nch: " + ch);
        System.out.println("ch: base: " + ch.getBase());
        System.out.println("ch: base area: " + ch.getBase().area());
        System.out.println("ch: total area: " + ch.area() + ", volume: " + ch.volume());
        System.out.println("ch length?: " + ch.length());


        //with composition:
        CylinderC cc = new CylinderC(0.1, 0.2, 2.5, 3.5);
        System.out.println("\ncc: " + cc);
        System.out.println("cc: base: " + cc.getBase());
        System.out.println("cc: base area: " + cc.getBase().area());
        System.out.println("cc: total area: " + cc.area() + ", volume: " + cc.volume());
        System.out.println("cc length?: " + cc.length()); //=> will result in compile error

    }
}