package teme.w05_oop2.ex1_cylinder;

public interface Cylinder {

    //[Cristi]: I've added this one, this is what tests expect in the interface!
    //and after this length() and getRadius() are not really needed here...
    Circle getBase();

    double length();

    double getHeight();

    double getRadius();


    default double area() {
        return 2 * Math.PI * Math.pow(getRadius(), 2) + getHeight() * length();
    }

    default double volume() {
        return Math.PI * Math.pow(getRadius(), 2) * getHeight();
    }

}
