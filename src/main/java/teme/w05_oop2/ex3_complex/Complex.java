package teme.w05_oop2.ex3_complex;

class Complex {

    private double real;
    private double img;

    Complex(double real, double img) {
        this.real = real;
        this.img = img;
    }

    private double getReal() {
        return real;
    }

    private double getImg() {
        return img;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Complex complex = (Complex) o;
        return Double.compare(complex.real, real) == 0 &&
                Double.compare(complex.img, img) == 0;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public String toString() {
        if (real == 0) {
            return img + "i";
        } else {
            if (img == 0) {
                return real + "";
            } else if (img < 0) {
                return real + "+ " + img + "i";
            }
        }
        return real + " + " + img + "i";
    }

    static Complex complex(double i, double i1) {
        return new Complex(i, i1);
    }

    public Complex add(Complex n2) {
        Complex temp = complex(0.0, 0.0);
        temp.real = this.real + n2.real;
        temp.img = this.img + n2.img;
        return (temp);
    }

    public Complex negate() {
        Complex temp = complex(0, 0);
        temp.real = -getReal();
        temp.img = -getImg();
        return temp;
    }

    public Complex multiply(Complex other) {
        Complex temp = complex(0, 0);
        temp.real = this.real * other.real - this.img * other.img;
        temp.img = this.real * other.img + this.img * other.real;
        return temp;
    }

    /**
     * Added just for running some manual tests
     */
    public static void main(String[] args) {

        Complex c1 = complex(1, 2);
        Complex c2 = complex(3, 4);

        //manual testing
        System.out.println("c1: " + c1 + ", c2: " + c2);
        System.out.println("!c1 = " + c1.negate());
        System.out.println("c1+c2 = " + c1.add(c2));
        System.out.println("c1*c2 = " + c1.multiply(c2));

    }

}
