package teme.w14_threads;

public class Ex1_ReverseGoodbye {
    public static void main(String[] args) {
        Runnable r = new Worker(1);
        Thread t = new Thread(r);
        t.start();
    }
}

class Worker implements Runnable {
    private int id;

    public Worker(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Hello from thread " + id);
        if (id <= 9) {
            Runnable r = new Worker(id + 1);
            Thread t = new Thread(r);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye from thread " + id);
    }
}