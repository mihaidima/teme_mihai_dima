package teme.w03_recap;

import java.util.Arrays;

class Ex5_DistinctWordCount {

    static String[] splitToWords(String text) {
        String[] arrRez2 = new String[0];
        String s1 = text.trim();
        if (s1.isEmpty()) {
            return arrRez2;
        }
        arrRez2 = s1.split("\\s+");
        return arrRez2;
    }

    static boolean contains(String[] wordsArray, String word) {
        for (String s : wordsArray) {
            if (s.equals(word)) {
                return true;
            }
        }
        return false;
    }

    static int distinctWordCount(String phrase) {
        int rez = 1;
        System.out.println(phrase);
        phrase = phrase.toLowerCase();
        System.out.println(phrase);
        String[] arrRezC = splitToWords(phrase);
        Arrays.sort(arrRezC);
        if (arrRezC.length == 0) {
            return 0;
        }
        for (int i = 1; i < arrRezC.length; i++) {
            if (!arrRezC[i].equals(arrRezC[i - 1])) {
                rez++;
            }
        }
        return rez;
    }


    //some manual tests
    public static void main(String[] args) {
        testIt("");
        testIt("abc");
        testIt("aa bb");
        testIt("aa  bb");
        testIt("  ");
        testIt(" aa  bb");
        testIt(" aa  bb aa");
        testIt("aa Cc bb  AA cc  ");
        testIt("  aa   bb cc  AA  aA ");
    }

    //helper method for manual tests
    private static void testIt(String phrase) {
        System.out.println("distinctWordCount('" + phrase + "') = " + distinctWordCount(phrase));
    }
}
