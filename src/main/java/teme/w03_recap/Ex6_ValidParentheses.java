package teme.w03_recap;

class Ex6_ValidParentheses {

    static boolean isValid(String expression) {
        if (expression.length() == 0) {
            return true;
        }
        int countD = 0;
        int countI = 0;
        String comp = "()";
        char[] rez = new char[expression.length()];
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == comp.charAt(0) && countD >= countI) {
                countD++;
                rez[i] = comp.charAt(0);
            }
            if (expression.charAt(i) == comp.charAt(1)) {
                countI++;
                rez[i] = comp.charAt(1);
            }
        }
        return (rez[0] == comp.charAt(0) && countD == countI)
                || (rez[0] != comp.charAt(1) && countD == countI);
    }

    /**
     * Some manual tests:
     */
    public static void main(String[] args) {
        testIt("");
        testIt("Abc 123");
        testIt("()");
        testIt("ab ()");
        testIt("x*(y+z), (2+3)");
        testIt("(1+(2+3))-((4))");
        testIt("(.()..((?))..)..");

        testIt("(..");
        testIt(")( abc ()");
        testIt("() )( ()");
    }

    private static void testIt(String s) {
        System.out.println();
        System.out.println("'" + s + "' - is valid? : " + isValid(s));
    }
}
