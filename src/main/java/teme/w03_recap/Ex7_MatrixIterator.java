package teme.w03_recap;

import java.util.Arrays;

class Ex7_MatrixIterator {

    static int sumAll(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                sum += matrix[i][j];
            }
        }
        return sum;
    }

    static int sumDiag1(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == j) {
                    sum += matrix[i][j];
                }
            }
        }
        return sum;
    }

    static int[] elementsDiag2(int[][] matrix) {
        int[] arr = new int[matrix.length];
        int k = 0;
        for (int i = 0; i < matrix.length; i++) {
            arr[k] = matrix[i][matrix.length - (i + 1)];
            k++;
        }
        return arr;
    }

    static int sumPerimeter(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if ((i == 0) || (i == (matrix.length - 1))) {
                    sum += matrix[i][j];
                } else {
                    if ((j == 0) || (j == (matrix.length - 1))) {
                        sum += matrix[i][j];
                    }
                }
            }
        }
        return sum;
    }


    //Helper function, jus for displaying the matrix
    private static void display(int[][] matrix) {
        String s = "";
        for (int[] row : matrix) {
            for (int elem : row) {
                s += elem + " ";
            }
            s += "\n";
        }
        System.out.println("\nThe matrix:\n" + s);
    }

    /**
     * Some manual tests:
     */
    public static void main(String[] args) {
        printInfo(new int[][]{});

        printInfo(new int[][]{
                {1}});

        printInfo(new int[][]{
                {1, 2},
                {3, 4}});

        printInfo(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}});

        printInfo(new int[][]{
                {1, 1, 1, 1},
                {1, 2, 3, 1},
                {1, 4, 5, 1},
                {1, 1, 1, 1}});

        printInfo(new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}});
    }

    private static void printInfo(int[][] m) {
        display(m);
        System.out.println("sumAll: " + sumAll(m));
        System.out.println("sumDiag1: " + sumDiag1(m));
        System.out.println("elementsDiag2: " + Arrays.toString(elementsDiag2(m)));
        System.out.println("sumPerimeter: " + sumPerimeter(m));
    }
}
