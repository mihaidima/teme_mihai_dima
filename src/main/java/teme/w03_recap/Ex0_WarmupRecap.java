package teme.w03_recap;

import java.util.Arrays;

class Ex0_WarmupRecap {

    static String ends(String text) {
        if (text.length() <= 2) {
            return text;
        }

        /*
        char a = text.charAt(0);
        char b = text.charAt(text.length() - 1);
        return "" + a + b;
        */

        char[] arr = text.toCharArray();
        String a = String.valueOf(arr[0]);
        String b = String.valueOf(arr[arr.length - 1]);
        return a + b;
    }

    static String middle(String text) {
        int len = text.length();
        if (text.length() <= 2) {
            return text;
        }
        if (len % 2 != 0) {
            int mid = (len - 1) / 2;
            return String.valueOf(text.charAt(mid));
        } else {
            int mid1 = (len - 1) / 2;
            int mid2 = mid1 + 1;
            String a = String.valueOf(text.charAt(mid1));
            String b = String.valueOf(text.charAt(mid2));
            return a + b;
        }
    }

    static String onlyUpper(String text) {
        String listUpper = "";
        /*for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                listUpper += c;
            }
        }*/
        for (char c : text.toCharArray()) {
            if (c >= 'A' && c <= 'Z') {
                listUpper += c;
            }
        }
        return listUpper;
    }

    static boolean contains(String text, char letter) {
        int i = 0;
        while (i < text.length()) {
            if (letter == text.charAt(i)) {
                return true;
            }
            i++;
        }
        return false;
    }

    static int count(String text, char letter) {
        int i = 0, count = 0;
        while (i < text.length()) {
            if (letter == text.charAt(i)) {
                count++;
            }
            i++;
        }
        return count;
    }

    static int countVowels(String s) {
        int countVowels = 0;
        String text = s.toLowerCase();
        for (char c : text.toCharArray()) {
            if ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u')) {
                countVowels++;
            }
        }
        return countVowels;
    }

    static int countVowels1(String s) {
        int count = 0;
        String text = s.toLowerCase();
        for (char c : text.toCharArray()) {
            if ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u')) {
                count++;
            }
        }
        return count;
    }

    static boolean isSorted(String text) {
        for (int i = 0; i < text.length() - 1; i++) {
            if (text.charAt(i) > text.charAt(i + 1)) {
                return false;
            }
        }
        return true;
    }

    static String sorted(String text) {
        char[] sort = text.toCharArray();
        Arrays.sort(sort);
        return String.valueOf(sort);
    }


    //some manual tests
    public static void main(String[] args) {
        System.out.println(ends(""));
        System.out.println(ends("a"));
        System.out.println(ends("ab"));
        System.out.println(ends("abcd"));

        System.out.println(middle(""));
        System.out.println(middle("a"));
        System.out.println(middle("ab"));
        System.out.println(middle("abc"));
        System.out.println(middle("abcd"));
        System.out.println(middle("abcde"));

        System.out.println(onlyUpper("A Fost OdaTa.."));

        System.out.println(contains("A Fost OdaTa..", 'f'));
        System.out.println(contains("A Fost OdaTa..", 'F'));

        System.out.println("\ncountVowels('Run, Forest, run!') = " + countVowels("Run, Forest, run!"));
        System.out.println("countVowels('I will..') = " + countVowels("I will.."));

        System.out.println("\nisSorted(\"abccd\"): " + isSorted("abccd"));
        System.out.println("isSorted(\"abec\"): " + isSorted("abec"));

        System.out.println("\nsorted(\"ba\"): " + sorted("ba"));
        System.out.println("\nsorted(\"badc\"): " + sorted("badc"));
    }
}
