package teme.w03_recap;

class Ex4_WordCount {

    static int wordCount(String phrase) {
        String s1 = phrase.trim();
        if (s1.isEmpty()) {
            return 0;
        }
        String[] arrRez = s1.split("\\s+");
        return arrRez.length;
    }

    /*
    static int stringArrange(String s1){
        char[] arr=s1.split("\\s+");
        return s1=Arrays.toString(arr);
        }*/

    //some manual tests
    public static void main(String[] args) {
        testIt("");
        testIt("abc");
        testIt("aa bb");
        testIt("aa  bb");
        testIt("  ");
        testIt(" aa  bb");
        testIt("aa bb  cc  ");
        testIt(" abc ");
        testIt(" ");
    }

    //helper method for manual tests
    private static void testIt(String phrase) {
        System.out.println("wordCount('" + phrase + "') = " + wordCount(phrase));
    }
}
