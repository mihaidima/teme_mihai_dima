package teme.w03_recap;

import java.util.Arrays;

class Ex11_MergeSort {

    static int[] sort(int[] array) {
        int j = 0;
        int k = 0;
        int[] rezArr = new int[array.length];
        System.arraycopy(array, 0, rezArr, 0, array.length);
        int valTrans;
        for (int i = 1; i < rezArr.length; i++) {
            valTrans = rezArr[i];
            j = i - 1;
            while (j >= 0 && rezArr[j] > valTrans) {
                rezArr[j + 1] = rezArr[j];
                j--;
            }
            rezArr[j + 1] = valTrans;
        }
        return rezArr;
    }

    public static void main(String[] args) {
        testSort(new int[]{});
        testSort(new int[]{1});
        testSort(new int[]{1, 2});
        testSort(new int[]{2, 1});
        testSort(new int[]{1, 3, 2});
        testSort(new int[]{1, 3, 2, 4});
        testSort(new int[]{3, 4, 1, 2});
        testSort(new int[]{4, 3, 2, 1});
        testSort(new int[]{5, 3, 4, 1, 2, 6});
        testSort(new int[]{2, 1, 3, 1, 3, 2});
    }

    private static void testSort(int[] arr) {
        System.out.println("Sorting " + Arrays.toString(arr) + " => " + Arrays.toString(sort(arr)));
    }
}
