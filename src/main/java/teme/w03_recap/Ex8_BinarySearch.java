package teme.w03_recap;

import java.util.Arrays;

class Ex8_BinarySearch {

    //--- a) FOR ITERATIVE VERSION ---//

    /**
     * Iterative version.
     * Searches for value x in whole given array, returns true if found
     */
    static boolean contains(int x, int[] array) {
        int beginIdx = 0;
        int lastIdx = array.length - 1;
        while (beginIdx <= lastIdx) {
            int midIdx = (beginIdx + lastIdx) / 2;
            if (array[midIdx] == x) {
                return true;
            } else if (array[midIdx] < x) {
                beginIdx = midIdx + 1;
            } else if (array[midIdx] > x) {
                lastIdx = midIdx - 1;
            }
        }
        return false;
    }


    //--- b) FOR RECURSIVE VERSION ---//

    /**
     * Recursive version.
     * Searches for value x in given array, but only in specified range (between beginIdx and endIdx, inclusive)
     * May call itself as needed (but with other params - different begin/end idx values)
     */
    private static boolean containsRec(int x, int[] array, int beginIdx, int endIdx) {
        if (beginIdx <= endIdx) {
            int midIdx = (beginIdx + endIdx) / 2;
            if (array[midIdx] == x) {
                return true;
            }
            if (array[midIdx] > x) {
                return containsRec(x, array, beginIdx, midIdx - 1);
            }
            if (array[midIdx] < x) {
                return containsRec(x, array, midIdx + 1, endIdx);
            }
        }
        return false;
    }

    //Optional 2_iterative:
    private static int containsIndex(int x, int[] array) {
        int beginIdx = 0;
        int lastIdx = array.length - 1;
        while (beginIdx <= lastIdx) {
            int midIdx = (beginIdx + lastIdx) / 2;
            if (array[midIdx] == x) {
                return midIdx;
            } else if (array[midIdx] < x) {
                beginIdx = midIdx + 1;
            } else if (array[midIdx] > x) {
                lastIdx = midIdx - 1;
            }
        }
        return -1;
    }

    //Optional 2_recursive:
    private static int containsRecIndex(int x, int[] array, int beginIdx, int endIdx) {
        if (beginIdx <= endIdx) {
            int midIdx = (beginIdx + endIdx) / 2;
            if (array[midIdx] == x) {
                return midIdx;
            }
            if (array[midIdx] > x) {
                return containsRecIndex(x, array, beginIdx, midIdx - 1);
            }
            if (array[midIdx] < x) {
                return containsRecIndex(x, array, midIdx + 1, endIdx);
            }
        }
        return -1;
    }

    /**
     * Just a helper function for the recursive version, for easier calling by clients/tests
     * (without needing to specify begin/end idx values)
     */
    static boolean containsRecStart(int x, int[] array) {
        return containsRec(x, array, 0, array.length - 1);
    }


    //--- FOR MANUAL TESTS ---//
    public static void main(String[] args) {
        System.out.println("\nITERATIVE - TRUE cases:");
        testContains(2, new int[]{2, 3, 4});
        testContains(2, new int[]{2, 3, 4});
        testContains(3, new int[]{2, 3, 4});
        testContains(4, new int[]{2, 3, 4});
        testContains(3, new int[]{1, 2, 3, 4, 5});
        System.out.println("\nITERATIVE - FALSE cases:");
        testContains(1, new int[]{});
        testContains(1, new int[]{2});
        testContains(1, new int[]{2, 3});
        testContains(7, new int[]{1, 2, 3, 4, 5});
        testContains(2, new int[]{1, 3, 5});

        System.out.println("\nRECURSIVE - TRUE cases:");
        testContainsRec(2, new int[]{2, 3, 4});
        testContainsRec(3, new int[]{2, 3, 4});
        testContainsRec(4, new int[]{2, 3, 4});
        testContainsRec(3, new int[]{1, 2, 3, 4, 5});
        System.out.println("\nRECURSIVE - FALSE cases:");
        testContainsRec(1, new int[]{});
        testContainsRec(1, new int[]{2});
        testContainsRec(1, new int[]{2, 3});
        testContainsRec(7, new int[]{1, 2, 3, 4, 5});
        testContainsRec(2, new int[]{1, 3, 5});


//manual test for optional
        System.out.println("\nITERATIVE - TRUE cases:");
        testcontainsIndex(2, new int[]{2, 3, 4});
        testcontainsIndex(3, new int[]{1, 2, 3, 4, 5});
        System.out.println("\nITERATIVE - FALSE cases:");
        testcontainsIndex(1, new int[]{});
        testcontainsIndex(1, new int[]{2, 3});
        testcontainsIndex(7, new int[]{1, 2, 3, 4, 5});


        System.out.println("\nRECURSIVE - TRUE cases:");
        testContainsRecIndex(2, new int[]{2, 3, 4});
        testContainsRecIndex(3, new int[]{1, 2, 3, 4, 5});
        System.out.println("\nRECURSIVE - FALSE cases:");
        testContainsRecIndex(1, new int[]{});
        testContainsRecIndex(1, new int[]{2});
        testContainsRecIndex(7, new int[]{1, 2, 3, 4, 5});


    }

    private static void testContains(int x, int[] array) {
        System.out.println(Arrays.toString(array) + " contains " + x + " ? : " + contains(x, array));
    }

    private static void testContainsRec(int x, int[] array) {
        System.out.println("recursive version: " + Arrays.toString(array) + " contains " + x + " ? : " + containsRecStart(x, array));
    }

    private static void testcontainsIndex(int x, int[] array) {
        System.out.println(Arrays.toString(array) + " contains " + x + " ? : " + contains(x, array));
    }

    private static void testContainsRecIndex(int x, int[] array) {
        System.out.println("recursive version: " + Arrays.toString(array) + " contains " + x + " ? : " + containsRecStart(x, array));
    }


    /*
    Algoritm (recursiv) de cautare valoare intr-un sir (de modificat pentru un sir String[] sau o variabila String => vezi aplicabilitate pe ex5)
b) FOR RECURSIVE VERSION ---//

    private static boolean containsRec(int x, int[] array, int beginIdx, int endIdx) {
        if (array.length == 0) {
            return false;
        }
        if (beginIdx > endIdx) {
            return false;
        } else {
            if (array[beginIdx] == x) {
                return true;
            } else if (array[endIdx] == x) {
                return true;
            }
        }
        return containsRec(x, array, beginIdx + 1, endIdx - 1);
    }

    //Optional 2_recursive:
    private static int containsRecIndex(int x, int[] array, int beginIdx, int endIdx) {
        if ((array.length == 0) || (beginIdx > endIdx)) {
            return -1;
        } else {
            if (array[beginIdx] == x) {
                return beginIdx;
            } else if (array[endIdx] == x) {
                return beginIdx;
            }
        }
        return -1;
    }

*/


}
