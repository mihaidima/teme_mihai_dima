package teme.w03_recap;

import java.util.Arrays;

class Ex1_ArrayOperations {

    static int[] append(int[] array, int elem) {
        int[] rezArr = new int[array.length + 1];
        for (int i = 0; i < array.length; i++)
            rezArr[i] = array[i];
        rezArr[rezArr.length - 1] = elem;
        return rezArr;
    }

    static int[] removeLast(int[] array) {
        if (array.length < 1) return array;
        int[] rezArr = new int[array.length - 1];
        for (int i = 0; i < rezArr.length; i++) {
            rezArr[i] = array[i];
        }
        return rezArr;
    }

    static int[] concat(int[] array1, int[] array2) {
        if ((array1.length < 1) && (array2.length < 1)) return array1;
        int[] concArr = new int[array1.length + array2.length];
        for (int i = 0; i < array1.length; i++)
            concArr[i] = array1[i];
        for (int i = 0; i < array2.length; i++)
            concArr[array1.length + i] = array2[i];
        return concArr;
    }

    static boolean contains(int[] array, int elem) {
        if (array.length < 1) return false;
        for (int value : array) if (value == elem) return true;
        return false;
    }

    static int indexOf(int[] array, int elem) {
        for (int i = 0; i < array.length; i++)
            if (array[i] == elem) return i;
        return -1;
    }

    static int lastIndexOf(int[] array, int elem) {
        int rez = -1;
        for (int i = 0; i < array.length; i++)
            if (array[i] == elem) rez = i;
        return rez;
    }


    static int[] filterPositives(int[] array) {
        if (array.length < 1) {
            return array;
        }
        int rez = 0;
        for (int value : array) {
            if (value >= 0) rez++;
        }
        int[] arrayRez = new int[rez];
        rez = 0;
        for (int value : array) {
            if (value >= 0) {
                arrayRez[rez] = value;
                rez++;
            }
        }
        return arrayRez;
    }


    //some manual tests
    public static void main(String[] args) {
        System.out.println(Arrays.toString(append(new int[]{1, 2, 3}, 4)));
        System.out.println(Arrays.toString(removeLast(new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(concat(new int[]{1, 2, 3}, new int[]{4, 5, 6})));

        System.out.println(contains(new int[]{1, 2, 3}, 7));
        System.out.println(contains(new int[]{1, 2, 3}, 2));

        System.out.println(indexOf(new int[]{10, 20, 30, 20}, 20));
        System.out.println(indexOf(new int[]{10, 20, 30, 20}, 70));

        System.out.println(lastIndexOf(new int[]{10, 20, 30, 20}, 20));
        System.out.println(lastIndexOf(new int[]{10, 20, 30, 20}, 70));

        System.out.println(Arrays.toString(filterPositives(new int[]{10, -20, 30, -40, 50})));
    }
}
