package teme.w01_intro;

class Ex11_KnightMoves {

    /**
     * Check if a given position is valid (within the bounds of a 8x8 chess table)
     *
     * @param row row number (valid range: 1..8)
     * @param col column number (valid range: 1..8)
     * @return true if both row and col values are in valid range, false otherwise
     */

    static boolean isValidPos(int row, int col) {
        if (1 <= row && row <= 8 && 1 <= col && col <= 8) return true;
        return false;
    }

    /**
     * Starting from a current position (row+column) and a given piece movement on table
     * (specified as a number of rows to move, and a number of columns, relative to current pos)
     * it will compute and return the destination position, encoded as a single String value.
     * <p>
     * If either the current or the destination position are invalid (outside the table)
     * it will return instead just an empty string.
     *
     * @param crtRow     current row (valid range: 1..8)
     * @param crtCol     current column (valid range: 1..8)
     * @param rowsToMove number of rows to move (a positive or negative number)
     * @param colsToMove number of rows to move (a positive or negative number)
     * @return the destination position as a string with format "(row,col)",
     * or empty string if source or destination positions are invalid
     */

    static String getDestinationIfValid(int crtRow, int crtCol, int rowsToMove, int colsToMove) {
        if (isValidPos(crtRow, crtCol)) {
            if (isValidPos(crtRow + rowsToMove, crtCol + colsToMove)) {
                return "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
            } else return "";
        }
        return "";
    }

    static String getAllValidDestinations(int crtRow, int crtCol) {
        String validDest = "";
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, -2, -1);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, -2, 1);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, -1, -2);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, -1, 2);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, 1, -2);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, 1, 2);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, 2, -1);
        validDest = validDest + getDestinationIfValid(crtRow, crtCol, 2, 1);
        return validDest;
    }

    /**
     * Given the current position of a knight (row+column), it will compute and return
     * the list of all VALID possible destinations to reach in 1 valid knight move.
     *
     * @param crtRow current row
     * @param crtCol current column
     * @return list of all valid destinations, in format "(row1,col1)(row2,col2)...(rowN,colN)"
     * (order of the destination cells doesn't count)
     */

/* first version
    static boolean getDestinationIfValidSubFunction(int crtRow, int crtCol, int rowsToMove, int colsToMove){
        if (isValidPos(crtRow, crtCol)) {
            return (isValidPos(crtRow + rowsToMove, crtCol + colsToMove));
        }
        return false;
    }

    static String getAllValidDestinations(int crtRow, int crtCol) {
        int rowsToMove = 0, colsToMove = 0;
        String pos1 = "", pos2 = "", pos3 = "", pos4 = "", pos5 = "", pos6 = "", pos7 = "", pos8 = "";
        rowsToMove = -2;
        colsToMove = -1;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos1 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = -2;
        colsToMove = 1;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos2 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = -1;
        colsToMove = -2;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos3 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = -1;
        colsToMove = 2;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos4 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = 1;
        colsToMove = -2;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos5 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = 1;
        colsToMove = 2;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos6 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = 2;
        colsToMove = -1;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos7 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        rowsToMove = 2;
        colsToMove = 1;
        if (getDestinationIfValidSubFunction(crtRow, crtCol, rowsToMove, colsToMove)) {
            pos8 = "(" + (crtRow + rowsToMove) + "," + (crtCol + colsToMove) + ")";
        }
        return pos1+pos2+pos3+pos4+pos5+pos6+pos7+pos8;
        }

 */


    /**
     * Main, just for running manual tests
     */
    public static void main(String[] args) {
        System.out.println(isValidPos(1, 9));
        System.out.println(isValidPos(2, 4));

        System.out.println(getDestinationIfValid(1, 1, -2, +1));
        System.out.println(getDestinationIfValid(1, 1, +2, +1));

        System.out.println(getAllValidDestinations(1, 1));
        System.out.println(getAllValidDestinations(2, 4));
        System.out.println(getAllValidDestinations(3, 3));
    }
}
