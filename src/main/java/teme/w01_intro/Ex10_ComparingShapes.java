package teme.w01_intro;

class Ex10_ComparingShapes {

    static final double PI = 3.141592; //approximate value of PI constant from math, can be used in your computation

    //--- CIRCLE ---//
    static double computeCircleArea(double radius) {
        return PI * radius * radius;
    }

    static double computeCircleLength(double radius) {
        return 2 * PI * radius;
    }

    //--- SQUARE ---//
    static double computeSquareArea(double side) {
        return side * side;
    }

    static double computeSquarePerimeter(double side) {
        return 4 * side;
    }

    //--- COMPARE SHAPES ---//

    /**
     * Computes and compares the AREAS of 2 different shapes
     *
     * @param radius radius of the circle shape
     * @param side   side of the square shape
     * @return "square" if the square shape has the bigger area, or "circle" otherwise
     */
    static String whichHasGreaterArea(double radius, double side) {
        return (computeCircleArea(radius) > computeSquareArea(side)) ? "circle" : "square";
    }

    /**
     * Computes and compares the PERIMETERS of 2 different shapes
     *
     * @param radius radius of the circle shape
     * @param side   side of the square shape
     * @return "square" if the square shape has the bigger perimeter, or "circle" otherwise
     */
    static String whichHasGreaterPerimeter(double radius, double side) {
        return (computeCircleLength(radius) > computeSquarePerimeter(side)) ? "circle" : "square";
    }

    //--- MAIN, just for manual tests ---//
    public static void main(String[] args) {
        System.out.println("Area of circle with r=5: " + computeCircleArea(5));
        System.out.println("Length of circle with r=5: " + computeCircleLength(5));

        System.out.println("Area of square with side=8: " + computeSquareArea(8));
        System.out.println("Perimeter of square with side=8: " + computeSquarePerimeter(8));

        System.out.println("circle(r=5) vs square(l=8) - has greater AREA: '" + whichHasGreaterArea(5, 8) + "'");
        System.out.println("circle(r=5) vs square(l=8) - has greater PERIMETER: '" + whichHasGreaterPerimeter(5, 8) + "'");
    }
}
