package teme.w01_intro;

class Ex6_DigitRemover {

    static int removeMiddleDigit(int n) {
        String n1, n2, n3, n4, n5, result = null;
        String lungSir = Integer.toString(n);
        if (n > 0 && lungSir.length() == 5) {
            n1 = String.valueOf((int) (n / 10000));
            n2 = String.valueOf(((int) n / 1000) % 10);
            n3 = String.valueOf(((int) n / 100) % 10);
            n4 = String.valueOf(((int) n / 10) % 10);
            n5 = String.valueOf((int) n % 10);
            result = n1 + n2 + n4 + n5;
            int m = Integer.parseInt(result);
            return m;
        }
        return -1;
    }

    //manual tests
    public static void main(String[] args) {
        //invalid:
        System.out.println(removeMiddleDigit(123));
        System.out.println(removeMiddleDigit(-12345));
        System.out.println(removeMiddleDigit(9999));
        System.out.println(removeMiddleDigit(100000));
        //valid:
        System.out.println(removeMiddleDigit(12345));
    }
}
