package teme.w13_jdbc.ex0_todo_notes.db.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class NotesDDL {

    public static void createTable() throws SQLException {
        String sql = "CREATE TABLE if not exists notes (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "DESCRIPTION VARCHAR(200) NOT NULL UNIQUE," +
                "PRIORITY INTEGER NOT NULL," +
                "DUEDATE DATE," +
                "STATE INTEGER NOT NULL" +
                ");";

        try (Connection conn = DBManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }
}