package teme.w13_jdbc.ex0_todo_notes.db.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Separate small class to build/provide the connection to the DB
 * to the rest of code (which should know care about the connection details)
 */
public class DBManager {
    final static String DB_FILE_NAME = "D:/01. Mihai Dima/07. Personale/08. Java/Data Base.db";

    static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:" + DB_FILE_NAME);
    }
}