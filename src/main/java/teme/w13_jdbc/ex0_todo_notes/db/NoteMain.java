package teme.w13_jdbc.ex0_todo_notes.db;

import teme.w13_jdbc.ex0_todo_notes.db.dto.NoteDTO;
import teme.w13_jdbc.ex0_todo_notes.db.dto.Priority;
import teme.w13_jdbc.ex0_todo_notes.db.dto.State;
import teme.w13_jdbc.ex0_todo_notes.db.service.NoteDAO;
import teme.w13_jdbc.ex0_todo_notes.db.service.NotesDDL;

import java.sql.SQLException;

public class NoteMain {
    public static void main(String[] args) throws SQLException {
        NotesDDL.createTable();
        NoteDAO dao = new NoteDAO();
        NoteDTO n1 = new NoteDTO(0, "O descriere",
                null, Priority.LOW, State.ACTIVE);
        dao.insert(n1);
        System.out.println(dao.getAll());

    }
}
