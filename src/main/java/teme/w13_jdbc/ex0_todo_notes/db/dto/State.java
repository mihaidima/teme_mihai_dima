package teme.w13_jdbc.ex0_todo_notes.db.dto;

public enum State {
    ACTIVE, COMPLETED;

    public static State getByOrdinal(int ordinal) {
        for (State s : values()) {
            if (s.ordinal() == ordinal)
                return s;
        }
        return null;
    }
}
