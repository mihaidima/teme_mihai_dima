package teme.w13_jdbc.ex0_todo_notes.db.dto;

import java.sql.Date;
import java.util.Objects;

public class NoteDTO {
    private int id;
    private String description;
    private Date dueDate;
    private Priority priority;
    private State state;

    public NoteDTO(int id, String description, Date dueDate, Priority priority, State state) {
        this.id = id;
        this.description = description;
        this.dueDate = dueDate;
        this.priority = priority;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoteDTO noteDTO = (NoteDTO) o;
        return id == noteDTO.id &&
                Objects.equals(description, noteDTO.description) &&
                Objects.equals(dueDate, noteDTO.dueDate) &&
                priority == noteDTO.priority &&
                state == noteDTO.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, dueDate, priority, state);
    }

    @Override
    public String toString() {
        return "NoteDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", dueDate=" + dueDate +
                ", priority=" + priority +
                ", state=" + state +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Priority getPriority() {
        return priority;
    }

    public State getState() {
        return state;
    }
}
