package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
    final static String DB_FILE_NAME = "D:/01. Mihai Dima/07. Personale/08. Java/Data Base.db";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:" + DB_FILE_NAME);
    }
}
