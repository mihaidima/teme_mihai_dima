package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PersonsDTO {
    private int id;
    private String first_name;
    private String last_name;
    private int age;
    private Gender gender;

    public PersonsDTO(int id, String first_name, String last_name, int age, Gender gender) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.gender = gender;
    }

    public static void insert(PersonsDTO person) {
        try (Connection connection = DBManager.getConnection()) {
            PersonsDAO.executeInsert(connection, person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void update(PersonsDTO person) {
        try (Connection connection = DBManager.getConnection()) {
            PersonsDAO.executeUpdate(connection, person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void delete(int id) {
        try (Connection connection = DBManager.getConnection()) {
            PersonsDAO.executeDelete(connection, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<PersonsDTO> select(int id) {
        List<PersonsDTO> result = new ArrayList<>();
        try (Connection connection = DBManager.getConnection()) {
            result.addAll(PersonsDAO.executeSelect(connection, id));
        } catch (SQLException e) {
            System.out.println("Error message is: " + e);
        }
        return result;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    static PersonsDTO convertFromLine(String line) {
        String[] parts = line.split(",");
        System.out.println(line);
        return new PersonsDTO(
                Integer.parseInt(parts[0].trim()),
                parts[1].trim(),
                parts[2].trim(),
                Integer.parseInt(parts[3].trim()),
                Gender.valueOf(parts[4].trim().toUpperCase()));
    }

    @Override
    public String toString() {
        return "PersonsDTO{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonsDTO that = (PersonsDTO) o;
        return id == that.id &&
                age == that.age &&
                Objects.equals(first_name, that.first_name) &&
                Objects.equals(last_name, that.last_name) &&
                gender == that.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, first_name, last_name, age, gender);
    }
}
