package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDao<T> {
    /**
     * Load list of objects from DB (all found)
     */
    public List<T> getAll() {
        try (Connection connection = DBManager.getConnection()) {
            return executeSelect(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Load a specific object from DB (by id)
     */
    public Optional<T> get(long id) {
        try (Connection connection = DBManager.getConnection()) {
            return Optional.ofNullable(executeSelect(connection, id));
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Add a new object to DB
     */
    public void insert(T object) {
        try (Connection connection = DBManager.getConnection()) {
            executeInsert(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update an existing object in DB
     */
    public void update(T object) {
        try (Connection connection = DBManager.getConnection()) {
            executeUpdate(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete an object from DB
     */
    public void delete(int id) {
        try (Connection connection = DBManager.getConnection()) {
            executeDelete(connection, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*--- To be implemented in specific way by each specific DTO! ---*/

    protected abstract List<T> executeSelect(Connection connection);

    protected abstract T executeSelect(Connection connection, long id);

    protected void executeInsert(Connection connection, T object) throws SQLException {
    }

    public abstract void executeUpdate(Connection connection, T object);

    protected abstract void executeDelete(Connection connection, int id);
}
