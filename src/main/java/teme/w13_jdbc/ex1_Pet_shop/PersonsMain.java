package teme.w13_jdbc.ex1_Pet_shop;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonsMain {

    public static void createTable() {
        PersonsDDL.createTable();
    }

    public static void insertFromFile() {
        PersonsDAO dao = new PersonsDAO();
        String basefolder = "D:/01. Mihai Dima/07. Personale/08. Java/Wantsome/teme_mihai_dima/src/main/java/teme/w13_jdbc/ex1_Pet_shop/";
        String inputFile = basefolder + "persons.csv";
        List<PersonsDTO> person = new ArrayList<>();
        try (Scanner sc = new Scanner(new File(inputFile))) {
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                try {
                    person.add(PersonsDTO.convertFromLine(line));
                } catch (Exception e) {
                    System.err.println("Skipped invalid line" + line + ", error was: " + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Input file not found, error: " + e);
        }
        for (PersonsDTO p : person) {
            PersonsDTO.insert(p);
        }
    }


    public static void insert(PersonsDTO person) {
        PersonsDTO.insert(person);
    }

    public static void personDel(int id) {
        PersonsDTO.delete(id);
    }

    public static void update(PersonsDTO person) {
        PersonsDTO.update(person);
    }

    public static List<PersonsDTO> select(int id) {
        return PersonsDTO.select(id);
    }

}
