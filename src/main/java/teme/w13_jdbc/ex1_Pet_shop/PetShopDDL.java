package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class PetShopDDL {
    public static void createTable() {
        String sql = "CREATE TABLE IF NOT EXIST PERSONS (" +
                "ID integer not null primary key," +
                "First_name varchar(100) not null," +
                "Last_name varchar(100) not null," +
                "age integer not null," +
                "gender varchar(1) not null" +
                ");";

        sql = sql + "CREATE TABLE IF NOT EXIST PET_TYPE(" +
                "ID integer not null primery key," +
                "Description varchar(50) not null);";

        sql = sql + "CREATE TABLE IF NOT EXIST PETS(" +
                "ID integer not null primary key," +
                "Type_id integer not null foreign key reference PET_TYPE(ID)," +
                "name varchar(100) not null," +
                "birt_date date not null," +
                "gender varchar(1) not null" +
                "person_ID integer not null foreign key reference PERSONS(ID));";

        try (Connection conn = DBManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
