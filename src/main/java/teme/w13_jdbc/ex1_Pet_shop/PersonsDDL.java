package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class PersonsDDL {
    public static void createTable() {
        String sql = "CREATE TABLE IF NOT EXISTS PERSONS (" +
                "ID integer not null primary key autoincrement," +
                "First_name varchar(100) not null," +
                "Last_name varchar(100) not null," +
                "age integer not null," +
                "gender varchar(1) not null" +
                ");";
        try (Connection conn = DBManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
