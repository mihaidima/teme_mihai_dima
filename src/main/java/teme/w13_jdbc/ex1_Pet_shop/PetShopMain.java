package teme.w13_jdbc.ex1_Pet_shop;

import static teme.w13_jdbc.ex1_Pet_shop.Gender.M;

public class PetShopMain {
    public static void main(String[] args) {
        PersonsMain.createTable();
        PersonsMain.insertFromFile();
        PersonsDTO p1 = new PersonsDTO(2, "Vasile", "Ionescu", 18, M);
        PersonsDTO p2 = new PersonsDTO(2, "Vasile", "Ionescu", 18, M);
        PersonsMain.personDel(2);
        PersonsMain.personDel(58);
        PersonsMain.insert(p1);
        PersonsMain.insert(p2);
        PersonsMain.update(p2);
        System.out.println(PersonsMain.select(15));
    }
}
