package teme.w13_jdbc.ex1_Pet_shop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonsDAO {

    protected List<PersonsDTO> executeSelect(Connection connection) {
        String sql = "select * from Persons";
        List<PersonsDTO> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                PersonsDTO p = new PersonsDTO(rs.getInt("id"),
                        rs.getString("First_name"),
                        rs.getString("Last_name"),
                        rs.getInt("Age"),
                        Gender.valueOf(rs.getString("Gender")));
                result.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static List<PersonsDTO> executeSelect(Connection connection, int id) {
        //todo
        return null;
    }

    public static void executeInsert(Connection connection, PersonsDTO person) {
        String sql = "INSERT INTO Persons (first_name, last_name, age, gender)" +
                " VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, person.getFirst_name());
            pStatement.setString(2, person.getLast_name());
            pStatement.setInt(3, person.getAge());
            pStatement.setString(4, person.getGender().toString());
            pStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void executeUpdate(Connection connection, PersonsDTO person) {
        String sql = "update Persons set first_name = ?, last_name = ? , age = ? , gender = ? " +
                " where id = ?";
        try {
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, person.getFirst_name());
            pStatement.setString(2, person.getLast_name());
            pStatement.setInt(3, person.getAge());
            pStatement.setString(4, person.getGender().toString());
            pStatement.setInt(5, person.getId());
            pStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void executeDelete(Connection connection, int id) {
        String sql = "delete from Persons where id = ?";
        try {
            PreparedStatement pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, id);
            pStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
