-- Script with the queries used in the jdbc example
-- For manual building/testing them, before moving them to java code.

drop table DOGS;

create table DOGS (
    ID integer  primary key autoincrement,
    NAME varchar not null,
    AGE integer
);

select * from dogs;

insert into dogs values ( null,'aZOR', 3);
insert into dogs(NAME, AGE) values ( 'Grivei', 2);