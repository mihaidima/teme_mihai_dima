package teme.w13_jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple example of using JDBC to access a DB table from java code.
 */
public class JdbcDogsExample {
    public static void main(String[] args) throws SQLException {

        manualTestsForDao();

        testOtherQueries();
    }

    private static void manualTestsForDao() throws SQLException {

        DogDao.deleteAll();
        System.out.println("Dogs from db before: " + DogDao.getAll());

        DogDao.insert(new DogDTO(-1, "Grivei", 3));
        DogDao.insert(new DogDTO(-1, "Azor", 5));
        DogDao.insert(new DogDTO(-1, "Rex", 7));
        DogDao.insert(new DogDTO(-1, "Negruta", 4));

        List<DogDTO> allDogs = DogDao.getAll();
        System.out.println("Dogs from db after: " + allDogs);

        int firstId = allDogs.get(0).getId();

        System.out.println(DogDao.get(firstId)); //=> Azor
        System.out.println(DogDao.get(-98)); //=> null
    }

    private static void testOtherQueries() throws SQLException {
        System.out.println("\nThe three oldest dogs: ");

        String sql = "select * from dogs order by age desc limit 3";

        //use try-with-resources: all these 3 kinds of resources (connection, statement, result set)
        //need to be properly closed
        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:test.db");
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {

            while (rs.next()) {

                //extract data from current row of result (the 3 relevant columns)
                int id = rs.getInt("id");
                String name = rs.getString("nAME");
                int age = rs.getInt("AGE");

                System.out.println("row: id= " + id + ", name= " + name + ", age= " + age);
            }
        }
    }
}

/**
 * Separate small class to build/provide the connection to the DB
 * to the rest of code (which should know care about the connection details)
 */
class DbManager {
    static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:test.db");
    }
}

/**
 * Class for DDL-type operations on DOGS table, like create/drop table...
 */
class DogDDL {

    static void createTable() throws SQLException {

        String sql = "CREATE TABLE IF NOT EXISTS DOGS (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME VARCHAR2 NOT NULL," +
                "AGE INTEGER" +
                ")";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    static void dropTable() { /* TODO */ }
}


/**
 * Class for DML operations for DOGS table
 * (row manipulation operations, like CRUD - create, read, update, delete)
 */
class DogDao {

    public static void deleteAll() throws SQLException {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute("delete from dogs");
        }
    }

    public static void insert(DogDTO dog) throws SQLException {

        String sql = "insert into dogs(name, age) values(?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement pst = conn.prepareStatement(sql)) {

            pst.setString(1, dog.getName());
            pst.setInt(2, dog.getAge());
            pst.execute();
        }
    }

    public static List<DogDTO> getAll() throws SQLException {

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from dogs order by name")) {

            List<DogDTO> dogs = new ArrayList<>();
            while (rs.next()) {

                int id = rs.getInt("id");
                String name = rs.getString("nAME");
                int age = rs.getInt("AGE");

                DogDTO dog = new DogDTO(id, name, age);

                dogs.add(dog);
            }
            return dogs;
        }
    }

    public static DogDTO get(int id) throws SQLException {
        try (Connection conn = DbManager.getConnection();
             PreparedStatement pst = conn.prepareStatement("SELECT * FROM dogs where id =?")) {

            pst.setInt(1, id);

            try (ResultSet rs = pst.executeQuery()) {

                if (rs.next()) {
                    return new DogDTO(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getInt("age"));
                }
            }

            return null;
        }
    }
}

/**
 * Simple DTO class to pack together in a single object the attributes for a dog,
 * corresponding to one row of DOGS table.
 */
class DogDTO {
    private final int id;
    private final String name;
    private final int age;

    DogDTO(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "DogDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
