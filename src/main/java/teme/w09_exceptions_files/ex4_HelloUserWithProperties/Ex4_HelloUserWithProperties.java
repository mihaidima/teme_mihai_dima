package teme.w09_exceptions_files.ex4_HelloUserWithProperties;


import java.io.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;


public class Ex4_HelloUserWithProperties {

    private final static String pathToPropFile = "D:/01. Mihai Dima/07. Personale/08. Java/Curs/Teme/" +
            "teme_mihai_dima/src/main/java/teme/w09_exceptions_files" +
            "/ex4_HelloUserWithProperties/";

    public static void main(String[] args) {
        // varianta de test, update pe useri diferiti cu map, dar ar trebui sa fac cu un fisier text
        String nameTest = "Mihai";
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter user name");
        String name = scan.nextLine();
        Map<String, String> userNames = new HashMap<>();
        if (userNames.containsKey(name)) {
            createPropFile(name, userNames.get(name));
        } else {
            userNames.put(name, "You have never accessed");
            createPropFile(name, userNames.getOrDefault(name, userNames.get(name)));
        }

        //local time fuctioneaza
        LocalTime localTime = LocalTime.now();
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String time = timeFormatter.format(localTime);
        userNames.put(name, time);

        printAll();
    }

    public static void createPropFile(String userName, String time) {
        try (OutputStream output = new FileOutputStream(
                String.format("%suser.properties", pathToPropFile))) {

            Properties prop = new Properties();

            // set the properties value
            prop.setProperty("Username", userName);
            prop.setProperty("time", String.valueOf(time));

            // save properties to project root folder
            prop.store(output, null);

            System.out.println(prop);

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    private static void printAll() {

        try (InputStream input = new FileInputStream(pathToPropFile
                + "user.properties")) {

            Properties prop = new Properties();
            prop.load(input);
            System.out.println("Hello, "
                    + prop.getProperty("Username")
                    + ", nice to see you again!");
            System.out.println("(last visit time: "
                    + prop.getProperty("time")
                    + " [hh:mm:ss])");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            System.out.println("Have a nice day!!");
        }
    }
}


/*        String baseFolder = "D:/01. Mihai Dima/07. Personale/08. Java/Curs/Teme/teme_mihai_dima/src/main/java/teme/w09_exceptions_files/ex4_HelloUserWithProperties/";
        Properties prop = new Properties();
        prop.load(new FileInputStream(baseFolder));
        String value = prop.getProperty("User");
        System.out.println(value);
        System.out.println(prop.entrySet());*/


/*
    private static String tryToGetParentFolderOfThisClass() {
        String path = Objects.requireNonNull(Ex4_HelloUserWithProperties.class.getClassLoader().getResource(".")).getPath();
        String pkg = Ex4_HelloUserWithProperties.class.getPackage().getName();
        String newPath = path.replace(
                "build" + separator + "classes" + separator + "java" + separator + "main",
                "src" + separator + "main" + separator + "java" + separator + pkg.replace(".", separator));
        //System.out.println("path: " + path);
        //System.out.println("package: " + pkg);
        System.out.println("computed base path: " + newPath);
        return newPath;
    }
*/
