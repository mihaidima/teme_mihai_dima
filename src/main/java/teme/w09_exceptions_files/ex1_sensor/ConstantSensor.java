package teme.w09_exceptions_files.ex1_sensor;

//TODO!
public class ConstantSensor implements Sensor {
    private int temp;

    public ConstantSensor(int value) {
        this.temp = value;
    }

    @Override
    public boolean isOn() {
        return true;
    }

    @Override
    public void on() {}

    @Override
    public void off() {}

    @Override
    public int measure() {
        return this.temp;
    }
}
