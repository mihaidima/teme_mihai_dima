package teme.w09_exceptions_files.ex1_sensor;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AverageSensor implements Sensor {
    private List<Sensor> listOfSensors = new ArrayList<>();

    public void addSensor(Sensor s) {
        listOfSensors.add(s);
    }

    @Override
    public boolean isOn() {
        List<Sensor> listOfSensorsOn = listOfSensors.stream()
                .filter(Sensor::isOn)
                .collect(Collectors.toList());
        return listOfSensors.size() == listOfSensorsOn.size();
    }

    @Override
    public void on() {
        for (Sensor i : listOfSensors) {
            i.on();
        }
    }

    @Override
    public void off() {
        for (Sensor i : listOfSensors) {
            i.off();
        }
    }

    @Override
    public int measure() throws MeasurementException {
        if (listOfSensors.size() == 0 || !isOn()) {
            throw new MeasurementException();
        }
        int sum = 0;
        for (Sensor i : listOfSensors) {
            sum += i.measure();
        }
        return sum / listOfSensors.size();
    }
}
