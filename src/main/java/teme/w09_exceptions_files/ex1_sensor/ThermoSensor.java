package teme.w09_exceptions_files.ex1_sensor;


import java.util.Random;

public class ThermoSensor implements Sensor {
    private boolean state;

    public ThermoSensor() {
        this.state = false;
    }

    @Override
    public boolean isOn() {
        return state;
    }

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public int measure() throws MeasurementException {
        if (!state) {
            throw new MeasurementException();
        }
        /*
        Pentru asa te rog sa ma ajuti cu un debug dc nu trece testul: thermoSensor_rangeOfMeasuredValues()
        double val = Math.random();
        return (val<0.5)? (int)((-1)*val*30)
                :(int)(val*30);
                Multumesc!
                */
        return (-30 + new Random().nextInt(61));
    }
}
