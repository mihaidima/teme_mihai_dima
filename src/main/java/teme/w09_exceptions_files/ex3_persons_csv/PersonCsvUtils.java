package teme.w09_exceptions_files.ex3_persons_csv;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class PersonCsvUtils {

    static List<Person> loadFromCsvFile(String fileName) {
        List<Person> persons = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                try {
                    persons.add(Person.convertFromLine(line));
                } catch (Exception e) {
                    System.err.println("skipped invalid line: '" + line + "', error was: " + e);
                }
            }
            System.out.println("found " + persons.size() + " persons");
        } catch (FileNotFoundException e) {
            System.out.println("File not found, error: " + e);
        }
        return persons;
    }

    static void writeToCsvFile(String fileName, List<Person> persons) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        for (Person p : persons) {
            String line = p.convertToLine();
            writer.write(line + "\n");
        }
        writer.close();
    }
}
