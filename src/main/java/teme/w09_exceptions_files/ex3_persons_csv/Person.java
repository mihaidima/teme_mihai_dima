package teme.w09_exceptions_files.ex3_persons_csv;

class Person implements Comparable<Person> {
    private final String name;
    private final String cnp;
    private final String phone;
    private final int height;

    Person(String name, String cnp, String phone, int height) {
        this.name = name;
        this.cnp = cnp;
        this.phone = phone;
        this.height = height;
    }

    int getHeight() {
        return height;
    }


    String convertToLine() {
        return name + "," +
                cnp + "," +
                phone + "," +
                height;
    }

    static Person convertFromLine(String line) {
        String[] parts = line.split(",");
        return new Person(
                parts[0].trim(),
                parts[1].trim(),
                parts[2].trim(),
                Integer.parseInt(parts[3].trim()));
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", cnp='" + cnp + '\'' +
                ", phone='" + phone + '\'' +
                ", height=" + height +
                '}';
    }

    @Override
    public int compareTo(Person other) {
        return this.name.compareTo(other.name);
    }
}
