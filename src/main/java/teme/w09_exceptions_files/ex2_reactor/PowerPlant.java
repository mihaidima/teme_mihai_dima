package teme.w09_exceptions_files.ex2_reactor;

class PowerPlant {
    boolean alarm = false;
    int targetPower;

    PowerPlant(int targetPower) {
        this.targetPower = targetPower;
    }

    int getTargetPower() {
        return targetPower;
    }

    void soundAlarm() {
        alarm = true;
        System.out.println("ALARM!"); //System.out.println("Winter is coming");
    }

    boolean hasAlarmSounded() {
        return alarm;
    }
}
