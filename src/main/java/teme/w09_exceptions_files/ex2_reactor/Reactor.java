package teme.w09_exceptions_files.ex2_reactor;

import java.util.Random;

class Reactor {
    private int reactorPower = 0;
    private int criticalPower;

    Reactor(int criticalPower) {
        this.criticalPower = criticalPower;
    }

    int getPower() {
        return reactorPower;
    }

    void increase() throws ReactorCriticalException {
        reactorPower += 1 + new Random().nextInt(100);
        if (reactorPower > criticalPower) {
            throw new ReactorCriticalException();
        }
    }

    void decrease() {
        reactorPower -= new Random().nextInt(100) + 1;
        if (reactorPower < 0) {
            reactorPower = 0;
        }
    }
}
