package teme.w09_exceptions_files.ex2_reactor;

class PlantController {
    PowerPlant nuclearPlant;
    Reactor nuclearReactor;

    PlantController(PowerPlant plant, Reactor reactor) {
        this.nuclearPlant = plant;
        this.nuclearReactor = reactor;
    }

    void run() {
        try {
            if
            (needAdjustment()) {
                adjust();
            }
        } catch (ReactorCriticalException e) {
            nuclearPlant.soundAlarm();
        }
    }

    boolean needAdjustment() {
        return (nuclearPlant.getTargetPower() - nuclearReactor.getPower()) > 10;
    }

    void adjust() throws ReactorCriticalException {
        while (needAdjustment()) {
            nuclearReactor.increase();
        }
    }

    void shutdown() {
        while (nuclearReactor.getPower() > 0) {
            nuclearReactor.decrease();
        }
    }
}
