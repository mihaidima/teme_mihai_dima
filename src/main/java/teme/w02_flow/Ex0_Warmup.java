package teme.w02_flow;

import java.util.Arrays;

class Ex0_Warmup {

    static String xTimes(String s, int n) {
        int i = 1;
        String res = "";
        while (i <= n) {
            res = res + s;
            i++;
        }
        return res;
    }

    static String xTimes_for(String s, int n) {
        int i = 1;
        String res1 = "";
        for (i = 1; i <= n; i++) {
            res1 = res1 + s;
        }
        return res1;
    }


    static int sumSquares(int n) {
        int i, sum = 0;
        for (i = 1; i <= n; i++) {
            sum = sum + i * i;
        }
        return sum;
    }


    static long factorial(int n) {
        long res = 1;
        for (int i = 1; i <= n; i++) {
            res = res * i;
        }
        return res;
    }

    static long factorial_down(int n) {
        long res_down = n;
        for (int i = n; i <= 1; i--) {
            res_down *= i;
        }
        return res_down;
    }

    static long factorial_down_recurs(int n) {
        if (n == 0) return 1;
        return n * factorial_down_recurs(n - 1);
    }

    static long factorialRec(int n) {
        if (n <= 1) return 1;
        return n * factorialRec(n - 1);
    }


    static byte dayOfWeek(String s) {
        String s1 = s.toLowerCase();
        byte dayNumber;
        switch (s1) {
            case "luni":
                dayNumber = 1;
                break;
            case "marti":
                dayNumber = 2;
                break;
            case "miercuri":
                dayNumber = 3;
                break;
            case "joi":
                dayNumber = 4;
                break;
            case "vineri":
                dayNumber = 5;
                break;
            case "sambata":
                dayNumber = 6;
                break;
            case "duminica":
                dayNumber = 7;
                break;
            default:
                dayNumber = -1;
        }
        return dayNumber;
    }

    //varianta propousa de Lucian, mai scurta ca si linii de cod
    static byte dayOfWeekCuReturn(String s) {
        switch (s.toLowerCase()) {
            case "luni":
                return 1;
            case "marti":
                return 2;
            case "miercuri":
                return 3;
            case "joi":
                return 4;
            case "vineri":
                return 5;
            case "sambata":
                return 6;
            case "duminica":
                return 7;
            default:
                return -1;
        }
    }

    static double sum(double[] arr) {
        double rez = 0;
        for (double n : arr) rez = rez + n;
        return rez;
    }

    static double avg(double[] arr) {
        return (sum(arr) / arr.length);
    }

    static double max(double[] arr) {
        if (arr.length == 0) return Double.NEGATIVE_INFINITY;
        double rez = arr[0];
        for (double v : arr)
            if (rez < v) {
                rez = v;
            }
        return rez;
    }


    static double sumPositives(double[] arr) {
        double rez = 0;
        for (double v : arr)
            if (v < 0) break;
            else rez = rez + v;
        return rez;
    }

    static void multiply(double[] arr, double x) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= x;
        }
        return;
    }


    public static void main(String[] args) {
        System.out.println("xTimes('abc',2)= '" + xTimes("abc", 2) + "'");
        System.out.println("xTimes('abc',2)= '" + xTimes("abc", 5) + "'");
        System.out.println("sumSquares(4)= " + sumSquares(4));

        System.out.println("\ndayOfWeek('luni') = " + dayOfWeek("luni"));
        System.out.println("dayOfWeek('DUMINICA') = " + dayOfWeek("DUMINICA"));
        System.out.println("dayOfWeek('invalid') = " + dayOfWeek("invalid"));

        System.out.println("\nfactorial(1) = " + factorial(1));
        System.out.println("factorial(5) = " + factorial(5));
        System.out.println("factorial(13) = " + factorial(13));
        System.out.println("factorialRec(1) = " + factorialRec(1));
        System.out.println("factorialRec(5) = " + factorialRec(5));
        System.out.println("factorialRec(13) = " + factorialRec(13));
        System.out.println("factorialDownRec(3) = " + factorial_down_recurs(3));


        double[] values1 = {1.1, 2.2, 3.3, 4.4};
        System.out.println("\nsum(" + Arrays.toString(values1) + ") = " + sum(values1));
        System.out.println("avg(" + Arrays.toString(values1) + ") = " + avg(values1));
        System.out.println("max(" + Arrays.toString(values1) + ") = " + max(values1));

        double[] values2 = {}; //an empty array
        System.out.println("\nsum(" + Arrays.toString(values2) + ") = " + sum(values2));
        System.out.println("avg(" + Arrays.toString(values2) + ") = " + avg(values2));
        System.out.println("max(" + Arrays.toString(values2) + ") = " + max(values2));

        double[] values3 = {1, 2, 3, -4, 5, 6};
        System.out.println("\nsumPositives(" + Arrays.toString(values3) + ") = " + sumPositives(values3));

        System.out.println("\ninitial array: " + Arrays.toString(values3));
        multiply(values3, 10);
        System.out.println("array after calling multiply() with factor 10: " + Arrays.toString(values3));
    }
}
