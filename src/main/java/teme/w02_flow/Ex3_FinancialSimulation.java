package teme.w02_flow;

class Ex3_FinancialSimulation {

    static int simulate(double initial, double interestPct, double target) {

        System.out.printf("\nSimulation started, with parameters: initial: %.2f, interest: %.2f%%, target: %.2f\n",
                initial, interestPct, target);
        /*if (initial>=target){
        System.out.println("You already have the amount"+initial);
        }*/
        double years = 0, expected = initial;
        while (target > expected) {
            expected = expected + (expected * (interestPct / 100));
            System.out.println("Year " + years + ": amount:" + expected + ": interest:" + interestPct / 100);
            years++;
        }
        System.out.println("You can retire in: " + String.valueOf(years) + " years");
        return (int) years;
    }

    public static void main(String[] args) {
        simulate(1000, 10, 900);
        simulate(1000, 10, 1000);
        simulate(1000, 10, 1100);
        simulate(1000, 10, 1600);

        simulate(1000, 2.5, 1500);
        simulate(1000, 0.1, 1500);
    }
}
