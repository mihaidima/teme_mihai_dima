package teme.w02_flow;

class Ex8_CheckArrayIsSorted {

    /**
     * @param arr - array of numbers to be sorted (given as varargs here, for easier testing)
     * @return true if array is sorted in ascending order, false otherwise
     */
    static boolean isSorted(int[] arr) {
        if (arr.length <= 1) return true;
        int v = 1, cresc = 1, descresc = 1;
        while (v < arr.length) {
            if (arr[v - 1] >= arr[v]) descresc++; // => condition needed to check if array is sorted
            if (arr[v - 1] <= arr[v]) cresc++;
            v++;
        }
        return cresc == arr.length;
    }

    public static void main(String[] args) {
        System.out.println(isSorted(new int[]{}));
        System.out.println(isSorted(new int[]{1}));
        System.out.println(isSorted(new int[]{1, 1, 2}));
        System.out.println(isSorted(new int[]{1, 2, 3, 5}));
        System.out.println(isSorted(new int[]{1, 2, 4, 3, 5}));
        //my test
        System.out.println(isSorted(new int[]{3, 2, 1}));
        System.out.println(isSorted(new int[]{1, 2, 4, 3}));
        System.out.println(isSorted(new int[]{1, 2, 4, 3, 5}));
        System.out.println(isSorted(new int[]{1, 2, 2}));
        System.out.println(isSorted(new int[]{1, 2, 2, 3}));
        System.out.println(isSorted(new int[]{2, 3, 3, 1}));
        System.out.println(isSorted(new int[]{3, 3, 2}));
        System.out.println(isSorted(new int[]{3, 3, 2, 2}));
        System.out.println(isSorted(new int[]{1, 2}));
        System.out.println(isSorted(new int[]{1, 4, 6}));

    }
}
