package teme.w02_flow;

class Ex10_ConsecutiveRepeats {

    static String onlyConsecutiveRepeating(int[] numbersArray) {
        StringBuilder arrayRez = new StringBuilder();
        if (numbersArray.length == 0) {
            return arrayRez.toString();
        }
        int pozElem = 0;
        int i = 0;
        while (i < numbersArray.length - 1) {
            if (numbersArray[i] == numbersArray[i + 1] && pozElem == 0) {
                arrayRez.append(" ").append(numbersArray[i]);
                pozElem++;
                i++;
            } else {
                if (pozElem > 0 && numbersArray[i] == numbersArray[i + 1]) {
                    i++;
                } else {
                    pozElem = 0;
                    i++;
                }
            }
        }
        return arrayRez.toString();
    }


    public static void main(String[] args) {
        System.out.println(onlyConsecutiveRepeating(new int[]{}));                          //expected: ''
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 1}));                      //expected: '1'
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 1, 1}));                   //expected: '1'
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 1, 2, 1, 1}));             //expected: '1 1'
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 4, 3, 4}));                //expected: ''
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 4, 4, 4, 3, 5, 5, 4, 4})); //expected: '4 5 4'
        System.out.println(onlyConsecutiveRepeating(new int[]{1, 4, 4, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9, 9})); //expected: '4 2 9'
    }
}
