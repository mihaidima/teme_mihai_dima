package teme.w02_flow;

class Ex2_FirstPowerAbove {

    static double firstPowerAbove(double base, double limit) {
        int x = 1;
        double calc = base, powerOfBase = 1;
        while (powerOfBase <= limit) {
            if (calc < 0) {
                calc = -calc;
            }
            powerOfBase = Math.pow(calc, x);
            ++x;
        }
        return powerOfBase;
    }

    public static void main(String[] args) {
        System.out.println(firstPowerAbove(2, 30));   //=> 30,   as: 2^5   = 32   > 30
        System.out.println(firstPowerAbove(2, 1000)); //=> 1024, as: 2^10  = 1024 > 1000
        System.out.println(firstPowerAbove(7, 0.1));  //=> 1,    as: 7^0   = 1    > 0.1
        System.out.println(firstPowerAbove(1.5, 2));  //=> 2.25, as: 1.5^2 = 2.25 > 2

        //My test
        System.out.println(firstPowerAbove(2.5, 10));
        System.out.println(firstPowerAbove(1.125, 4));
        System.out.println(firstPowerAbove(-7, 10));
        System.out.println(firstPowerAbove(-2, 5));
        System.out.println(firstPowerAbove(-3, 1000)); // printeaza 2187 > 1000
    }
}
