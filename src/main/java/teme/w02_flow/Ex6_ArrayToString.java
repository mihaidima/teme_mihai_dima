package teme.w02_flow;

class Ex6_ArrayToString {

    static String arrayToString(double[] array) {
        if (array.length == 0) return "[" + "]";
        else {
            if (array.length == 1)
                return "[" + String.format("%.2f", array[0]) + "]"; //("%s, %s", rez, df.format(array[0]))+ "]";
        }
        String rez = String.format("%.2f", array[0]);
        int v = 1;
        while (v < array.length) {
            rez = rez + ", " + String.format("%.2f", array[v]);
            v++;
        }
        return "[" + rez + "]";
    }

    public static void main(String[] args) {
        System.out.println(arrayToString(new double[]{}));
        System.out.println(arrayToString(new double[]{7}));
        System.out.println(arrayToString(new double[]{1.1, 2.345, 3.0}));
        System.out.println(arrayToString(new double[]{1.11, 2.22, 3.33, 44.44}));
    }
}
