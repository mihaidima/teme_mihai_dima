package teme.w02_flow;

import java.util.Arrays;

class Ex9_ReverseArray {

    static void reverse(String[] arr) {
        if (arr.length <= 1) return;
        String var;
        int i = 0;
        while (i <= (arr.length - 1) / 2) {
            var = arr[i];
            arr[i] = arr[(arr.length - 1) - i];
            arr[(arr.length - 1) - i] = var;
            i++;
        }
    }

    static String[] reversedCopy(String[] arr) {
        int lungArrayNou = 0, v, i;
        if (arr.length == 0) return arr;
        v = 0;
        while (v < arr.length) {
            lungArrayNou++;
            v++;
        }
        String[] rezReverse = new String[lungArrayNou];
        i = lungArrayNou - 1;
        v = 0;
        while (v < arr.length) {
            rezReverse[i] = arr[v];
            v++;
            i--;
        }
        return rezReverse;
    }


    //helper method - swaps the elements at 2 given indices, in an array

    static void reverseSwapPosition(String[] arr) {
        if (arr.length <= 1) {
            return;
        }
        int i = 0;
        while (i < (arr.length - 1) / 2) {
            swapPositions(arr, i, (arr.length - 1) - i);
            i++;
        }
    }

    static void swapPositions(String[] arr, int i, int j) {
        String elemTanz = arr[i];
        arr[i] = arr[j];
        arr[j] = elemTanz;
    }


    public static void main(String[] args) {
        String[] arr = {"aa", "bb", "cc", "dd", "ee"};
        System.out.println("initial:  " + Arrays.toString(arr));

        System.out.println("a reversed copy: " + Arrays.toString(reversedCopy(arr)));

        System.out.println("initial after making the copy (should be unchanged): " + Arrays.toString(arr));

        reverse(arr);
        System.out.println("initial after reverse(): " + Arrays.toString(arr));
    }
}
