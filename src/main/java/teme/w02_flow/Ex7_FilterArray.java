package teme.w02_flow;

import java.util.Arrays;

class Ex7_FilterArray {

    static int[] onlyOdd(int[] array) {
        int lungArrayNou = 0;
        int v;
        if (array.length == 0) return array;
        v = 0;
        while (v < array.length) {
            if (array[v] % 2 > 0) lungArrayNou++;
            v++;
        }
        int[] rezOnlyOdd = {};
        if (lungArrayNou > 0) rezOnlyOdd = new int[lungArrayNou];
        v = 0;
        lungArrayNou = 0;
        while (v < array.length) {
            if (array[v] % 2 > 0) {
                rezOnlyOdd[lungArrayNou] = array[v];
                lungArrayNou++;
            }
            v++;
        }
        return rezOnlyOdd;
    }

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(
                        onlyOdd(new int[]{1, 2, 3, 4, 5})));
    }
}
