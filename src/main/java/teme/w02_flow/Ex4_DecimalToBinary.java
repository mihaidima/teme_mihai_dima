package teme.w02_flow;

class Ex4_DecimalToBinary {

    static String decimalToBinary(int n) {
        if (n == 0) {
            return String.valueOf(n);
        } else {
            String bin = "";
            int i = n, rez;
            while (i > 0) {
                rez = i % 2;
                i = i / 2;
                bin = String.format("%s%s", String.valueOf((int) rez), bin); //bin = String.valueOf((int) rez)+bin; - before IntelliJ hint
            }
            return bin;
        }
    }

    /**
     * Just for manual testing
     */
    public static void main(String[] args) {
        System.out.println(decimalToBinary(0));
        System.out.println(decimalToBinary(1));
        System.out.println(decimalToBinary(2));
        System.out.println(decimalToBinary(3));
        System.out.println(decimalToBinary(4));
        System.out.println(decimalToBinary(10));
    }
}
