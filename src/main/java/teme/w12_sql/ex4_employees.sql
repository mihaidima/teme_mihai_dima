-- ===========================
-- Ex4. Employees (SOLUTION)
-- ===========================

-- !NOTE: before working on your solution, check/run the separate INIT script!

pragma foreign_keys = on; --make sure FK checks are on

---------------------------------------------------------
--##### I. SELECT:
--
-- 1. Write a query to get the details of the employees where the length
--    of the first name is greater than or equal to 8.
--    - ___Hint__: use `LENGTH` function._
select *
from employees e
where length (e.FIRST_NAME)>=8;

-- 2. Write a query that displays the first name and the length of the first
--    name for all employees whose name starts with letters 'A', 'J' or 'M'.
--    Give each column an appropriate label. Sort the results by the employees'
--    first names.
select e.FIRST_NAME as First_Name, length (e.FIRST_NAME) as Name_length
from employees e
where e.FIRST_NAME like 'A%' or e.FIRST_NAME like 'J%' or e.FIRST_NAME like 'M%'
order by e.FIRST_NAME;

-- 3. Write a query to display the name (first_name, last_name), salary
--    and TAX (15% of salary) of all employees.
select e.FIRST_NAME||e.LAST_NAME as Name, e.SALARY, SALARY*0.15 as Tax
from employees e

-- 4. Write a query to list the department ID and name of all the departments
--    where no employee is working.
--    - ___Hint__: if you are using the `NOT IN` operator for this one, note that
--      it may not in IntelliJ, but works ok in DBeaver. (note that 'not in' is
--      not the only way to do it, you may try a join too..)_
select d.ID, d.NAME
from departments d, employees e
where e.DEPARTMENT_ID = null
--NOT IN version:
--select d.ID, d.NAME
--from departments d, employees e
--NOT IN (SELECT e.DEPARTMENT_ID from employees e)
;



-- 5. Write a query to display the name (first_name, last_name) and salary
--    of all the employees who earns more than of Mr. Bell.
select e.FIRST_NAME ||" " || e.LAST_NAME as Name, e.SALARY
from employees e
where SALARY > (select e.SALARY from employees e where  e.LAST_NAME like "Bell");


-- 6. Write a query to display the employee ID, first name, last name, salary
--    of all employees whose salary is above average for their departments.
select e.ID, e.FIRST_NAME, e.LAST_NAME, e.SALARY
from employees e
where e.SALARY > ( select avg(e1.SALARY) from employees e1 where e1.DEPARTMENT_ID=e.DEPARTMENT_ID)
order by e.DEPARTMENT_ID
;

--##### II. JOIN:
-- 1. Write a query to display the job history that were done by any employee
--    who is currently earning more than 10000.
select  *
from job_history jh
join employees e on jh.EMPLOYEE_ID=e.ID
where e.SALARY > 10000;


-- 2. Write a query to display job name, employee name, and the difference
--    between the salary of the employee and minimum salary for the job.
select e.JOB_ID, e.first_name, salary-min_salary as "Salary - Min_Salary"
from employees e
join jobs j on j.ID=e.JOB_ID;


-- 3. Write a query to display the job name and average salary of employees.
select j.NAME, avg(e.SALARY)
from jobs j
join employees e on j.ID=e.JOB_ID
group by j.ID;


-- 4. Write a query to display the department name, manager name, and city.
select d.NAME, e.FIRST_NAME, l.CITY
from departments d
join employees e on d.MANAGER_ID = e.MANAGER_ID and e.DEPARTMENT_ID=d.ID
join locations l on l.ID = d.LOCATION_ID;

-- 5. Write a query to display the department ID and name and first name of manager.
select d.ID, d.NAME, e.FIRST_NAME
from departments d
join employees e on e.ID=d.MANAGER_ID;

-- 6. Write a query to find the employee ID, job name, number of days between
--    end date and start date for all jobs in department 110 from job history.
select jh.EMPLOYEE_ID, j.NAME,jh.END_DATE-jh.START_DATE as Days
from job_history jh
join departments d on d.ID=jh.DEPARTMENT_ID
join jobs j on j.ID=jh.JOB_ID
where d.ID=110


--##### III. UPDATE:
-- 1. Write a query to update the portion of the phone_number in the employees
--    table, within the phone number the substring '590' will be replaced by '111'.
--    - ___Hint__: use the `REPLACE` function._
--    - _Note that REPLACE may not work when run directly from IntelliJ
--      (with DB Navigator plugin) but should work ok from DBeaver._
update employees
set PHONE_NUMBER = replace (PHONE_NUMBER, "%590%", "%111%")
where PHONE_NUMBER like "%590%"
;


-- 2. Write a query to append '@wantsome.ro' to email field (but avoid appending
--    it twice).
--    - ___Hint__: use the concatenate operator: `||`_
update  employees
set EMAIL = email || "@wantsome.ro"
where EMAIL not like "%@wantsome.ro"



-- 3. Write a SQL statement to change salary of employee to 8000 with ID is 105,
--    if the existing salary is less than 5000.
update employees
set SALARY = 8000
where SALARY<5000;



-- 4. Write a SQL statement to change job ID of employee with ID = 118 to
--    SH_CLERK if the employee belongs to the department with ID 30 and
--    the existing job ID does not start with SH.
update employees
set JOB_ID = "SH_CLERK"
where DEPARTMENT_ID = 30 and JOB_ID not like "SH_CLERK";



-- 5. Write a SQL statement to increase the salary of employees under the
--    departments 40, 90 and 110 according to the new company rules: salary
--    will be increased by a factor of 2 for the department 40,
--    3 for department 90 and 10 for the department 110 and the rest
--    of the departments will remain the same.
--    - ___Hint__: read about [CASE expression in SQLite](https://www.sqlitetutorial.net/sqlite-case)_
--    - _Note that `CASE` may not work when run directly from IntelliJ (with
--       DB Navigator plugin), but should work ok from DBeaver. Also, as this may
--       even make your whole script un-runnable in IntelliJ, you should write and
--       test it in DBeaver, but then comment it out for IntelliJ._
--    - _If you cannot do it in a single statement (using CASE), try at least to
--       solve it, even if using multiple statements._
UPDATE employees SET SALARY = (case department_id
    when 40 then salary*2
    when 90 then salary*4
    when 110 then salary*10
    else salary
    end)
where department_ID = 40 or department_id=90 or department_id = 110
;



---------------------------------------------------------
