-- ========================
-- Ex1. Traders (warm-up)
-- ========================

pragma foreign_keys = on; --make sure FK checks are on

----------------------------------------------
-- Init script - tables + sample data
-----------------------------------------------

-- Drop existing tables? (run this manually if needed: select whole text of the row, without the '--' prefix, then: Alt+X)
-- drop table traders;
-- drop table transactions;

----------------------------------------------
-- TODO: YOUR SOLUTION BELOW
----------------------------------------------
CREATE TABLE traders (
ID INTEGER PRIMARY KEY,
name VARCHAR(80) NOT NULL,
city VARCHAR(80) NOT NULL
);

CREATE TABLE transactions(
ID INTEGER PRIMARY KEY,
year INTEGER NOT NULL,
value INTEGER NOT NULL,
trader_ID INTEGER REFERENCES traders(ID)
);

insert into traders values (1, 'Tony', 'Milan');
insert into traders values (2, 'John', 'Cambridge');
insert into traders values (3, 'Oliver', 'Cambridge');
insert into traders values (4, 'Ion', 'Iasi');

insert into transactions values(1, 2011, 100, 1);
insert into transactions values(2, 2012, 80, 1);
insert into transactions values(3, 2013, 120, 1);
insert into transactions values(4, 2011, 50, 3);
insert into transactions values(5, 2010, 130, 2);
insert into transactions values(6, 2011, 70, 2);
insert into transactions values(7, 2012, 90, 2);
insert into transactions values(8, 2011, 60, 4);
insert into transactions values(9, 2012, 140, 4);

SELECT * FROM transactions t2 WHERE "year"=2011 ORDER By value;

SELECT DISTINCT city FROM traders t2;

SELECT * FROM traders t2 WHERE city='Cambridge' ORDER BY name DESC;

SELECT name FROM traders t2 ORDER by name;
SELECT name as nume FROM traders t2 ORDER by name;

SELECT count() FROM traders t2 WHERE city='Milan';
SELECT count() FROM traders t2 WHERE city='Cambridge';

UPDATE traders SET city='Cambridge' WHERE city='Milan';
SELECT count() FROM traders t2 WHERE city='Cambridge';

SELECT * FROM traders t2;
SELECT * FROM transactions t2;

SELECT MAX(value), MIN(value) FROM transactions t2;
SELECT MAX(value) as 'Maximum value', MIN(value) as 'Minimum value',  AVG(value)as 'Average' FROM transactions t2;